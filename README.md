# Elucidating the patterns of pleiotropy and its biological relevance in maize

Name: Merritt Khaipho-Burch

Contact: mbb262@cornell.edu

Project Start: 2019-06-27

Readme Last Updated: 2023-01-09


## Introduction: 
We want to determine the extent to which pleiotropy affects maize phenotypic variation through roughly 110k different phenotypes in the Nested Association Mapping (NAM) population and the Goodman Association panel (282 panel, GAP). To do this we consistently remapped these phenotypes in rTASSEL using v4 Hapmap 3.2.1 SNPs and then determined how many unique traits (our metric for pleiotropy) in different population-trait categories impacted different biological annotations such as gene expression, open chromatin, sequence conservation, and GO terms. 

### Rationale:
Past pleiotropy studies in maize and related crops have either 1) focused on a limited number of traits, 2) had low resolution from QTL mapping, 3) had large variation and little comparability due to differences in QTL or GWAS models.  Here we collect roughly 110k different traits, perform GWAS on these traits with 25M imputed NAM and Goodman Panel SNPs, and consistently call GWAS results in order to examine pleiotropy. 

### Repo layout:
- /src: contains R and shell scripts
- /data: local only folder containing small files
- /images: images used for publication 

### Where can you find this paper?
BioRxiv: [Elucidating the patterns of pleiotropy and its biological relevance in maize](https://doi.org/10.1101/2022.07.20.500810)