# ---------------------------------------------------------------
# Author.... Merritt Burch
# Contact... mbb262@cornell.edu
# Date...... 2020-06-03 
#
# Description 
#   - Gather vcf files with 282 Hapmap3.2.1 genotypes
#	- filter to common set of SNPs between Ames, NAM, and goodman panel
#	- Merge files, export, use to calculate PCs in R
# ---------------------------------------------------------------

# Get data from cbsu
scp -r mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/goodman282 /workdir/mbb262
scp mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/ames/imputed/ames_for_pca/ames_for_pca_merged_position_list.json.gz /workdir/mbb262
scp mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/ames/imputed/ames_for_pca/ames_for_pca_merged.vcf.gz /workdir/mbb262/goodman282/filtered_for_pca
gunzip /workdir/mbb262/goodman282/filtered_for_pca/ames_for_pca_merged.vcf.gz

WORKINGPATH=/workdir/mbb262/goodman282

# Subset 282 files by the Ames Position List by chromosome
for CHROM in {1..10}
do
  echo "I am on chr ${CHROM}"

  /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug ${WORKINGPATH}/filtered_for_pca/debug_subset_282_by_ames_sites_chr${CHROM}.log \
    -Xmx250g \
    -maxThreads 40 \
    -importGuess ${WORKINGPATH}/hmp321_282_agpv4_merged_chr${CHROM}_imputed_goodman282.vcf.gz -noDepth \
    -FilterSiteBuilderPlugin \
    -positionList /workdir/mbb262/ames_for_pca_merged_position_list.json.gz \
    -endPlugin \
    -export ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr${CHROM}.vcf.gz \
    -exportType VCF

  echo "I just finished chr ${CHROM}"
done


# Gzip all files
gzip *.vcf

# Merge all subsetted files
/programs/tassel-5-standalone/run_pipeline.pl \
  -debug ${WORKINGPATH}/filtered_for_pca/debug_merge_nam_forPCA.txt \
  -Xmx250g \
  -maxThreads 40 \
  -fork1 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr11.vcf.gz -noDepth \
  -fork2 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr21.vcf.gz -noDepth \
  -fork3 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr31.vcf.gz -noDepth \
  -fork4 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr41.vcf.gz -noDepth \
  -fork5 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr51.vcf.gz -noDepth \
  -fork6 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr61.vcf.gz -noDepth \
  -fork7 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr71.vcf.gz -noDepth \
  -fork8 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr81.vcf.gz -noDepth \
  -fork9 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr91.vcf.gz -noDepth \
  -fork10 -importGuess ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_chr101.vcf.gz -noDepth \
  -combine11 -input1 -input2 -input3 -input4 -input5 -input6 -input7 -input8 -input9 -input10 \
  -mergeGenotypeTables \
  -export ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_allChroms.vcf.gz \
  -exportType VCF

gunzip ${WORKINGPATH}/filtered_for_pca/goodman282_by_ames_sites_for_pca_allChroms.vcf.gz

