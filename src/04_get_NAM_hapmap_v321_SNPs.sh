#!/bin/bash

# ---------------------------------------------------------------
# Author.... Merritt Burch
# Contact... mbb262@cornell.edu
# Date...... 2020-02-10 
#
# Description 
#   - Gather vcf files with NAM founders and all taxa
#   - Beagle impute all taxa
#	- Prepare SNPs for calculating PCs 
#	- need to filter sites based on imputation accuracy
# ---------------------------------------------------------------

# ----------------------------
#     NAM data subsetting
# ----------------------------

# Subset nam founders
for CHROM in {1..10}
do
  echo "Start data subsetting on chromosome ${CHROM}"
  bcftools index -c /workdir/mbb262/AGPv4/hmp321_282_agpv4_merged_chr${CHROM}.imputed.vcf.gz
  bcftools view -S /workdir/mbb262/nam_founders/nam_founder_taxa.txt hmp321_282_agpv4_merged_chr${CHROM}.imputed.vcf.gz \
    -Oz -o \
    /workdir/mbb262/nam_founders/hmp321_282_agpv4_merged_chr${CHROM}_imputed_nam_founders.vcf.gz
  echo "End data subsetting on chromosome ${CHROM}"
done

# Subset NAM founders to only have B73 and Mo17
for CHROM in {1..10}
do
  echo "Start data subsetting on chromosome ${CHROM}"
  # bcftools index -c /workdir/mbb262/AGPv4/hmp321_282_agpv4_merged_chr${CHROM}.imputed.vcf.gz
  bcftools view -S /workdir/mbb262/ibm_taxa/ibm_nam_founders.txt hmp321_282_agpv4_merged_chr${CHROM}_imputed_nam_founders.vcf.gz -Oz -o /workdir/mbb262/nam_founders/hmp321_282_agpv4_merged_chr${CHROM}_imputed_ibm_nam_founders.vcf.gz
  echo "End data subsetting on chromosome ${CHROM}"
done

# Subset out ibm taxa from all unimputed NAM taxa
for CHROM in {1..10}
do
  echo "Start data subsetting on chromosome ${CHROM}"
  bcftools index -c /workdir/mbb262/unimputed/ZeaGBSv27_AGPv4_NAM_chr${CHROM}.vcf.gz
  bcftools view -S /workdir/mbb262/ibm_taxa/ibm_taxa.txt ZeaGBSv27_AGPv4_NAM_chr${CHROM}.vcf.gz -Oz -o /workdir/mbb262/ibm_taxa/ZeaGBSv27_AGPv4_NAM_chr${CHROM}_ibm_taxa.vcf.gz
  echo "End data subsetting on chromosome ${CHROM}"
done


# ------------------
# Beagle imputation
# ------------------


# Imputation on only IBM population
for CHROM in {1..10}
do
  echo "Start Beagle Imputation on chromosome ${CHROM}"
  java \
    -Xmx500g \
    -jar /home/mbb262/bioinformatics/beagle5.1_25Nov19.28d.jar \
    gt=/workdir/mbb262/unimputed/ibm_taxa_unimputed/ZeaGBSv27_AGPv4_NAM_chr${CHROM}_ibm_taxa.vcf.gz \
    ref=/workdir/mbb262/imputed/nam/nam_founders_only_ibms/hmp321_282_agpv4_merged_chr${CHROM}_imputed_ibm_nam_founders.vcf.gz \
    ne=1000 \
    nthreads=63 \
    out=/workdir/mbb262/imputed/nam/ibm_beagle_imputed/hmp321_282_agpv4_merged_chr${CHROM}_imputed_ibm_nam_founders
  echo "End Beagle imputation on chromosome ${CHROM}"
done


# Index vcfs, merge them together in sorted fashion, export, gzip
# merge step takes forever
for CHROM in {1..10}
do
  echo "Start merge on chromosome ${CHROM}"
  # Imputed IBM lines
  bcftools index -c /workdir/mbb262/imputed/nam/ibm_beagle_imputed/hmp321_282_agpv4_merged_chr${CHROM}_imputed_ibm_nam_founders.vcf.gz
  # All NAM taxa, imputed
  bcftools index -c /workdir/mbb262/imputed/nam/nam_only_imputed/AGPv4_NAM_chr${CHROM}.imputed.vcf.gz
  # Merged indexed impouted NAM taxa and imputed IBM taxa
  bcftools merge -m all /workdir/mbb262/imputed/nam/ibm_beagle_imputed/hmp321_282_agpv4_merged_chr${CHROM}_imputed_ibm_nam_founders.vcf.gz /workdir/mbb262/imputed/nam/nam_only_imputed/AGPv4_NAM_chr${CHROM}.imputed.vcf.gz -o /workdir/mbb262/imputed/nam/combined_ibm_nam_all_imputed/nam_ibm_imputed_chr${CHROM}.vcf
  # bgzip merged file together
  bgzip < /workdir/mbb262/imputed/nam/combined_ibm_nam_all_imputed/nam_ibm_imputed_chr${CHROM}.vcf > /workdir/mbb262/imputed/nam/combined_ibm_nam_all_imputed/nam_ibm_imputed_chr${CHROM}.vcf.gz
  echo "End merge on chromosome ${CHROM}"
done


# Copy files over from blfs1 onto local machine
scp -r mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/ /workdir/mbb262/genotypes/nam/imputed/
scp -r mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/nam/imputed/ibm_beagle_imputed/hmp321_282_agpv4_merged_chr7_imputed_ibm_nam_founders.vcf.g* /workdir/mbb262/genotypes/nam/imputed/ibm_beagle_imputed
scp -r mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/nam/imputed/all_nam/AGPv4_NAM_chr7.imputed.vcf.gz /workdir/mbb262/genotypes/nam/imputed/all_nam
scp -r mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/ames/imputed/ /workdir/mbb262/genotypes/ames/


# Filter files based on imputation accuracy (for GWAS SNPs and later PCs)
for CHROM in {1..10}
do
  echo "Start data filtering on chromosome ${CHROM}"

  # Index files for bcftools
  # bcftools index -c /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/nam_ibm_imputed_chr${CHROM}.vcf.gz

  # Filter merged files (all NAM and IBM imputed) based on imputation accuracy, allele freq, and place in own folder
  bcftools filter -e 'DR2<=0.8 & AF<0.99 & AF>0.01' \
      /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/nam_ibm_imputed_chr${CHROM}.vcf.gz \
      -o \
      /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_DR2_0.8/nam_ibm_imputed_filteredDR2_0.8_chr${CHROM}.vcf.gz

  echo "End data filtering on chromosome ${CHROM}"
done

# Transfer back to blfs1
scp -r /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/nam_ibm_imputed_chr7.vcf* \
      mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools
scp -r /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_DR2_0.8/ \
      mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools


# --------------------------
# Filter Ames SNPs to ~40k
# --------------------------


# Filter files for calculating PCs (want ~40k SNPs), running on screen -r 776
for CHROM in {1..10}
do
  echo "Start data filtering on chromosome ${CHROM}"

  # Filter Ames SNPs based on imputation accuracy, allele freq, LD, and minimum count of alleles
  bcftools view -c 2500 /workdir/mbb262/genotypes/ames/imputed/AGPv4_Ames_chr${CHROM}.imputed.vcf.gz | \
      bcftools +prune -l 0.2 -w 500kb -e 'AF<0.99 & AF>0.01 & DR2<=0.9' \
      -o /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr${CHROM}_imputed_filtered_PCA_subset.vcf

  echo "End data filtering on chromosome ${CHROM}"
done

# Merge Ames files together with tassel
/programs/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/debug.txt \
    -Xmx200g \
    -maxThreads 25 \
    -fork1 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr1_imputed_filtered_PCA_subset.vcf \
    -fork2 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr2_imputed_filtered_PCA_subset.vcf \
    -fork3 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr3_imputed_filtered_PCA_subset.vcf \
    -fork4 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr4_imputed_filtered_PCA_subset.vcf \
    -fork5 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr5_imputed_filtered_PCA_subset.vcf \
    -fork6 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr6_imputed_filtered_PCA_subset.vcf \
    -fork7 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr7_imputed_filtered_PCA_subset.vcf \
    -fork8 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr8_imputed_filtered_PCA_subset.vcf \
    -fork9 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr9_imputed_filtered_PCA_subset.vcf \
    -fork10 -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/AGPv4_Ames_chr10_imputed_filtered_PCA_subset.vcf \
    -combine11 -input1 -input2 -input3 -input4 -input5 -input6 -input7 -input8 -input9 -input10 \
    -mergeGenotypeTables \
    -export /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/ames_for_pca_merged.vcf.gz \
    -exportType VCF


# Get position list (ist of sites to subset NAM SNPs)
/programs/tassel-5-standalone/run_pipeline.pl \
   -debug \
   -Xmx200g \
   -importGuess /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/ames_for_pca_merged.vcf \
   -GetPositionListPlugin \
   -endPlugin \
   -export /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/ames_for_pca_merged_position_list


# Subset NAM files by the Ames Position List by chromosome
for CHROM in {1..10}
do
  echo "I am on chr ${CHROM}"

  /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/debug_subset_nam_by_ames_sites_chr${CHROM}.log \
    -Xmx250g \
    -maxThreads 37 \
    -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/nam_ibm_imputed_chr${CHROM}.vcf.gz -noDepth \
    -FilterSiteBuilderPlugin \
    -positionList /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/ames_for_pca_merged_position_list.json.gz \
    -endPlugin \
    -export /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr${CHROM}.vcf \
    -exportType VCF

  echo "I just finished chr ${CHROM}"
done


# Merge all subsetted files
/programs/tassel-5-standalone/run_pipeline.pl \
  -debug /workdir/mbb262/genotypes/ames/imputed/ames_for_pca/debug_merge_nam_forPCA.txt \
  -Xmx250g \
  -maxThreads 25 \
  -fork1 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr11.vcf \
  -fork2 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr21.vcf \
  -fork3 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr31.vcf \
  -fork4 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr41.vcf \
  -fork5 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr51.vcf \
  -fork6 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr61.vcf \
  -fork7 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr71.vcf \
  -fork8 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr81.vcf \
  -fork9 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr91.vcf \
  -fork10 -importGuess /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_chr101.vcf \
  -combine11 -input1 -input2 -input3 -input4 -input5 -input6 -input7 -input8 -input9 -input10 \
  -mergeGenotypeTables \
  -export /workdir/mbb262/genotypes/nam/imputed/combined_ibm_nam_all_imputed_bcftools/filtered_for_pca/nam_by_ames_sites_for_pca_allChroms.vcf \
  -exportType VCF

# Try merging with picard
# merge step takes forever
for CHROM in {1..10}
do
  echo "Start formatting files on chromosome ${CHROM}"
  # Unzip files
  gunzip /workdir/mbb262/filtered_DR2_0.8/nam_ibm_imputed_filteredDR2_0.8_chr${CHROM}.vcf.gz
  # Bgzip files for bcftools
  bgzip < /workdir/mbb262/filtered_DR2_0.8/nam_ibm_imputed_filteredDR2_0.8_chr${CHROM}.vcf > /workdir/mbb262/filtered_DR2_0.8/nam_ibm_imputed_filteredDR2_0.8_chr${CHROM}.vcf.gz
  # Index files
  bcftools index -c /workdir/mbb262/filtered_DR2_0.8/nam_ibm_imputed_filteredDR2_0.8_chr${CHROM}.vcf.gz
  echo "End formatting files on chromosome ${CHROM}"
done

# Merge files together
bcftools concat /workdir/mbb262/filtered_DR2_0.8/*vcf.gz \
  -Oz -o /workdir/mbb262/filtered_DR2_0.8/nam_ibm_imputed_filteredDR2_0.8_merged_all_chroms.vcf.gz

# Get out of bgzip format into regular gzip format
bgzip -d /workdir/mbb262/filtered_DR2_0.8/nam_ibm_imputed_filteredDR2_0.8_merged_all_chroms.vcf.gz

# run gzip
gzip /workdir/mbb262/filtered_DR2_0.8/nam_ibm_imputed_filteredDR2_0.8_merged_all_chroms.vcf

