# ------------------------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2021-06-03 
# Updated... 2022-12-08
#
# Description 
# Format gene list from Ensembl plants to get genic and intergenic ranges
# and also single SNP ranges
# ------------------------------------------------------------------------------

# File originally from maize gdb
# ftp://ftp.ensemblgenomes.org/pub/plants/release-50/gff3/zea_mays/Zea_mays.B73_RefGen_v4.49.gff3.gz


## Load packages ----
library(data.table)
library(magrittr)
library(dplyr)
library(tidyverse)


# ------------------------------------------------------------------------------
# Make geneic and intergenic intervals
# ------------------------------------------------------------------------------

## Load data ----
gff <- ape::read.gff("~/git_projects/pleiotropy/data/Zea_mays.B73_RefGen_v4.49.gff3.gz", 
                     na.strings = c(".", "?"), 
                     GFF3 = TRUE)

# separate chromosome information
chroms <- gff %>% 
  filter(type == "chromosome", seqid %in% c(1,2,3,4,5,6,7,8,9,10)) %>% 
  select("seqid", "start", "end")

# Remove extra columns
gff <- gff %>% 
  filter(type == "gene", source == "gramene", seqid %in% c(1,2,3,4,5,6,7,8,9,10)) %>% 
  select("seqid", "start", "end")

# Turn into a GRanges object
genic_ranges <- GenomicRanges::makeGRangesFromDataFrame(gff, keep.extra.columns=TRUE) # make GRanges object

# merge overlapping ranges
genic_ranges <- GenomicRanges::reduce(genic_ranges)

# Create intergenic ranges
intergenic_ranges <- GenomicRanges::gaps(genic_ranges)

# Turn back into dataframes 
genic_ranges <- as.data.frame(genic_ranges) %>% select(-"strand")
intergenic_ranges <- as.data.frame(intergenic_ranges) %>% select(-"strand")

# Expand genic intervals to ends of chromosomes by grabbing largest end interval
temp <- genic_ranges %>% 
  group_by(seqnames) %>%
  summarise(max = max(end, na.rm=TRUE))

# Merge together ends of chroms with genic ends, subset, rename
temp <- merge(temp, chroms, by.x = "seqnames", by.y = "seqid") %>% 
  select("seqnames", "max", "end")
colnames(temp) <- c("seqnames", "start", "end")
temp$start <- temp$start + 1
temp$width <- temp$end - temp$start
intergenic_ranges <- rbind(intergenic_ranges, temp) # add to intergenic ranges

# sort both intergenic and genic data
intergenic_ranges <- intergenic_ranges %>% 
  group_by(seqnames) %>%
  arrange(start, .by_group = TRUE)

genic_ranges <- genic_ranges %>% 
  group_by(seqnames) %>%
  arrange(start, .by_group = TRUE)

# Look at sizes of intervals, some intergenic are large
summary(intergenic_ranges$width) # mean = 51513
summary(genic_ranges$width) # mean = 4286

# Create names for all ranges
intergenic_ranges$rr_id <- paste0("intergenic_rr_", seq_len(nrow(intergenic_ranges))) # add intergenic names to ranges
genic_ranges$rr_id <- paste0("genic_rr_", seq_len(nrow(genic_ranges)))
colnames(genic_ranges) <- c("seqnames", "start", "end", "width", "rr_id")

# Summary of all ranges 
tmp <- rbind(genic_ranges, intergenic_ranges) 
summary(tmp$width) # 27902

# merge genic and intergenic intervals
genic_ranges <- rbind(genic_ranges, intergenic_ranges) %>% 
  select(-"width") %>% 
  data.table::as.data.table()

dim(genic_ranges)


# Write to file to use elsewhere
write.csv(genic_ranges,
          file = "~/Downloads/genic_intergenic_intervals_b73_v4.49.csv",
          row.names = FALSE,
          quote = FALSE)


# Saving the file within
# /data1/users/mbb262/results/pleiotropy/interval_data/genic_intergenic_intervals_b73_v4.49.csv


# ------------------------------------------------------------------------------
# Make single SNP intervals
# ------------------------------------------------------------------------------

# Load packages
library(dplyr)
library(data.table)

# Shell code
# 4M sites is too many (it causes a lm machine to fill up on intermediate files)
# Drop down to 2M sites
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/genotypes/goodman282/*gz ./
# for FILE in *.gz
# do
# /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
# -debug /workdir/mbb262/subsample_debug.txt -Xmx200g -maxThreads 60 \
# -importGuess ./$FILE -noDepth -filterAlign -subsetSites 100000 \
# -genotypeSummary site -export ${FILE}_subsample_sites.txt
# done

# Load all subsampled sites into r, rbind them, subset columns
temp <- list.files("/workdir/mbb262/snps/", pattern = "*.txt", full.names = TRUE)
sub_snps <- lapply(temp, data.table::fread) %>% 
  rbindlist() %>% 
  select("Chromosome", "Physical Position")

# Add other identifiers, change column names
sub_snps$end <- sub_snps$`Physical Position`
sub_snps$rr_id <- paste0("rr_", seq(1,nrow(sub_snps)))
colnames(sub_snps) <- c("seqnames", "start", "end", "rr_id")

# Check
dim(sub_snps)
head(sub_snps)
tail(sub_snps)

# Export
data.table::fwrite(sub_snps, "/workdir/mbb262/snp_intervals_b73_v4.49.csv")
