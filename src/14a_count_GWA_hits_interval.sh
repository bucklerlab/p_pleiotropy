#!/bin/bash

# ---------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2021-06-09
# Modified.. 2022-02-11
#
# Description 
# Use Zack's custom association count funcition to count the 
# number of GWA hits falling wihtin an interval
# ---------------------------------------------------------------

# Usage from Zack's readme
/tassel-5-standalone/run_pipeline.pl \
    -debug \
    -Xmx 100G \
    -CountAssociationsPlugin \
    -intervals intervalFile.csv \
    -gwasResults fastAssociationResultFile.csv \
    -outputCountFile outputCountFile.txt \
    -endPlugin

# Readme from CountAssociationsPlugin
# intervalFile.csv is the Comma separated file holding the non-overlapping intervals.  Must have the 
# following columns in the following order: seqnames,start,end,rr_id
    
# fastAssociationResultFile.csv is the Fast Association file output in a Comma-separated format.  
# Needs the following columns in the following order: Trait,p,seqid,snp_coord
    
# outputCountFile.txt is the tab delimited output file from this plugin.  This will count how many 
# GWAS hits are found in each interval for each trait.


# -------------------------------
# Make counts for filtered data
# -------------------------------

# Get filtered (no need to rearrage, already in correct order from filtering to p = 0.00001
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/results/pleiotropy/gwa_results_goodman_panel/filtered_goodman
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/results/pleiotropy/gwa_rtassel_all_nam_traits/filtered_nam
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/results/pleiotropy/gwa_results_goodman_panel_kremling_maffilter/filtered_kremling


# make directories
mkdir /workdir/mbb262/output_counts
mkdir /workdir/mbb262/output_counts/nam_filtered_count
mkdir /workdir/mbb262/output_counts/goodman_filtered_count
mkdir /workdir/mbb262/output_counts/kremling_filtered_count


# Do count for NAM
cd /workdir/mbb262/results/nam_all
for FILE in *.csv
do
    echo "Start analyzing trait file: ${FILE}"
    /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/output_counts/nam_filtered_count/debug.txt \
    -Xmx200g \
    -CountAssociationsPlugin \
    -intervals /workdir/mbb262/genic_intergenic_intervals_b73_v4.49.csv \
    -gwasResults $FILE \
    -outputCountFile /workdir/mbb262/output_counts/nam_filtered_count/${FILE}_outputCount.txt \
    -endPlugin
done


# Do count for goodman panel - regular + metabolite traits
cd /workdir/mbb262/results/goodman_all/processed_results
for FILE in *.csv
do
    echo "Start analyzing trait file: ${FILE}"
    /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/output_counts/goodman_filtered_count/debug.txt \
    -Xmx200g \
    -CountAssociationsPlugin \
    -intervals /workdir/mbb262/genic_intergenic_intervals_b73_v4.49.csv \
    -gwasResults $FILE \
    -outputCountFile /workdir/mbb262/output_counts/goodman_filtered_count/${FILE}_outputCount.txt \
    -endPlugin
done


# Do count for goodman panel expression filtered
cd /workdir/mbb262/results/kremling_filtered/processed_results/

# Goodman panel expression filtered as a parallel command --> works much faster!
# find all files in directory ending with csv, save to file
find /workdir/mbb262/results/kremling_filtered/processed_results/*.csv -type f > kremling_filt_files.list

# run count association plugin in parallel
/programs/parallel/bin/parallel -j 10 "/home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl -debug /workdir/mbb262/output_counts/kremling_filtered_count/debug.txt -Xmx200g -CountAssociationsPlugin -intervals /workdir/mbb262/genic_intergenic_intervals_b73_v4.49.csv -gwasResults {} -outputCountFile /workdir/mbb262/output_counts/kremling_filtered_count_test/outputCount_{/.}.txt -endPlugin" :::: /workdir/mbb262/results/kremling_filtered/processed_results/kremling_filt_files.list


# ---------------------------------------------
# Permuted data counts are now embedded within:
#  15_mapping_permutations_all_populations.R
# ---------------------------------------------

