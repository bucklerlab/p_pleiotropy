#!/bin/bash

# ---------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2022-12-09
# Modified.. 2022-12-09
#
# Description 
# Use Zack's custom association count funcition to count the 
# number of GWA hits falling wihtin an interval. Interval for this
# script means a single SNP. SNPs were subsampled to 4M sites
# genome-wide.
# ---------------------------------------------------------------

# Usage from Zack's readme
# /tassel-5-standalone/run_pipeline.pl \
#     -debug \
#     -Xmx 100G \
#     -CountAssociationsPlugin \
#     -intervals intervalFile.csv \
#     -gwasResults fastAssociationResultFile.csv \
#     -outputCountFile outputCountFile.txt \
#     -endPlugin

# Readme from CountAssociationsPlugin
# intervalFile.csv is the Comma separated file holding the non-overlapping intervals.  Must have the 
# following columns in the following order: seqnames,start,end,rr_id
    
# fastAssociationResultFile.csv is the Fast Association file output in a Comma-separated format.  
# Needs the following columns in the following order: Trait,p,seqid,snp_coord
    
# outputCountFile.txt is the tab delimited output file from this plugin.  This will count how many 
# GWAS hits are found in each interval for each trait.

# -----------------------------
# Randomly subsample intervals
# -----------------------------

# Note, see the genic_intergenic_intervals.R script for further processing
# 1M snps because 4 M makes 8.3 terabytes of intermediate files, 1M = ~2T
mkdir -p /workdir/mbb262/snps
cd /workdir/mbb262/snps
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/genotypes/goodman282/*gz ./
for FILE in *.gz
do
    /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
        -debug /workdir/mbb262/subsample_debug.txt \
        -Xmx200g \
        -maxThreads 60 \
        -importGuess ./$FILE \
        -noDepth -filterAlign \
        -subsetSites 100000 \
        -genotypeSummary site \
        -export ${FILE}_subsample_sites.txt
done

# Format these files within: 09_genic_intergenic_snp_intervals.R



# -------------------------------
# Make counts for filtered data
# -------------------------------

# make directories
mkdir -p /workdir/mbb262/results/nam_all
mkdir -p /workdir/mbb262/results/goodman_field/processed_results
mkdir -p /workdir/mbb262/results/goodman_metabolite/processed_results
mkdir -p /workdir/mbb262/results/kremling_filtered/processed_results
mkdir /workdir/mbb262/output_counts
mkdir /workdir/mbb262/output_counts/nam_filtered_count
mkdir /workdir/mbb262/output_counts/goodman_filtered_count
mkdir /workdir/mbb262/output_counts/kremling_filtered_count
mkdir /workdir/mbb262/output_counts/goodman_metabolite_filtered_count

# Get filtered (no need to rearrage, already in correct order from filtering to p = 0.00001
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/gwa_rtassel_all_nam_traits/nam_all/processed_results/*.gz /workdir/mbb262/results/nam_all
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/gwa_results_goodman_panel/goodman_all/processed_results/*.gz /workdir/mbb262/results/goodman_all
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/gwa_results_goodman_panel_kremling_maffilter/kremling_all/processed_results/*.gz /workdir/mbb262/results/kremling_filtered/processed_results

# Unzip files
cd /workdir/mbb262/results/nam_all
cd /workdir/mbb262/results/goodman_all
cd /workdir/mbb262/results/kremling_filtered/processed_results
for FILE in *.gz
do
    bgzip -d --threads 60 ${FILE}
done

# Split metabolite traits and field traits
cd /workdir/mbb262/results/goodman_all
mv *Zhou_2019* /workdir/mbb262/results/goodman_metabolite/processed_results
mv ./* /workdir/mbb262/results/goodman_field/processed_results


# Do count for NAM
cd /workdir/mbb262/results/nam_all

# Do on a single test file
# /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
#     -Xmx200g  \
#     -debug /workdir/mbb262/debug.txt \
#     -CountAssociationsPlugin \
#     -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
#     -gwasResults chrom_8_buckler_2009_processed_00001_filtered.csv \
#     -outputCountFile /workdir/mbb262/test_count.csv \
#     -endPlugin
# vim /workdir/mbb262/debug.txt
# vim /workdir/mbb262/test_count.csv
cd /workdir/mbb262/results/nam_all
for FILE in *.csv
do
    echo "Start analyzing trait file: ${FILE}"
    /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/output_counts/nam_filtered_count/debug.txt \
    -Xmx200g \
    -CountAssociationsPlugin \
    -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
    -gwasResults $FILE \
    -outputCountFile /workdir/mbb262/output_counts/nam_filtered_count/${FILE}_outputCountSNP.txt \
    -endPlugin
done

# Check if the order of traits is the same
for f in ./*outputCountSNP.txt; do
    
    if [ -z "$first_intervals" ]; then
        first_intervals=$( awk  '{ print $1 }' $f )
    else
        intervals=$( awk  '{ print $1 }' $f )
        
        if [ "$first_intervals" != "$intervals" ]; then
            echo "File $f does not match interval order!"
        fi
    fi
done


# Do count for goodman panel field traits
cd /workdir/mbb262/results/goodman_field/processed_results
for FILE in *.csv
do
    echo "Start analyzing trait file: ${FILE}"
    /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/output_counts/goodman_filtered_count/debug.txt \
    -Xmx200g \
    -CountAssociationsPlugin \
    -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
    -gwasResults $FILE \
    -outputCountFile /workdir/mbb262/output_counts/goodman_filtered_count/${FILE}_outputCountSNP.txt \
    -endPlugin
done

# Do count for goodman panel metabolite traits
cd /workdir/mbb262/results/goodman_metabolite/processed_results
for FILE in *.csv
do
    echo "Start analyzing trait file: ${FILE}"
    /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/output_counts/goodman_filtered_count/debug.txt \
    -Xmx200g \
    -CountAssociationsPlugin \
    -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
    -gwasResults $FILE \
    -outputCountFile /workdir/mbb262/output_counts/goodman_metabolite_filtered_count/${FILE}_outputCountSNP.txt \
    -endPlugin
done


# Do count for goodman panel expression filtered
cd /workdir/mbb262/results/kremling_filtered/processed_results/

# Do on a single test trait
/home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/debug.txt \
    -Xmx200g \
    -CountAssociationsPlugin \
    -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
    -gwasResults chrom_9_fast_assoc_results_L3Base_Kremling_2018_reformatted_zack.csv \
    -outputCountFile /workdir/mbb262/temp_output.txt \
    -endPlugin
vim /workdir/mbb262/debug.txt
vim /workdir/mbb262/temp_output.txt

# Goodman panel expression filtered as a parallel command --> works much faster!
# find all files in directory ending with csv, save to file
find /workdir/mbb262/results/kremling_filtered/processed_results/*.csv -type f > kremling_filt_files.list

# run count association plugin in parallel
/programs/parallel/bin/parallel -j 15 "/home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl -debug /workdir/mbb262/output_counts/kremling_filtered_count/debug.txt -Xmx200g -CountAssociationsPlugin -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv -gwasResults {} -outputCountFile /workdir/mbb262/output_counts/kremling_filtered_count/outputCountSNP_{/.}.txt -endPlugin" :::: /workdir/mbb262/results/kremling_filtered/processed_results/kremling_filt_files.list


# ---------------------------------------------
# Permuted data counts for intervals are here:
#  15_mapping_permutations_all_populations.R
#
# SNP-level counts are below
# ---------------------------------------------


# make directories
mkdir -p /workdir/mbb262/results/nam_permuted
mkdir -p /workdir/mbb262/results/goodman_permuted
mkdir -p /workdir/mbb262/results/goodman_field_permuted/processed_results
mkdir -p /workdir/mbb262/results/goodman_metabolite_permuted/processed_results
mkdir -p /workdir/mbb262/results/kremling_filtered_permuted/processed_results

mkdir /workdir/mbb262/output_counts
mkdir /workdir/mbb262/output_counts/nam_permuted_count
mkdir /workdir/mbb262/output_counts/goodman_permuted_count
mkdir /workdir/mbb262/output_counts/kremling_permuted_count
mkdir /workdir/mbb262/output_counts/goodman_metabolite_permuted_count

# Get filtered (no need to rearrage, already in correct order from filtering to p = 0.00001
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/gwa_rtassel_all_nam_traits/nam_permuted/processed_results/*.gz /workdir/mbb262/results/nam_permuted
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/gwa_results_goodman_panel/goodman_permuted/processed_results/*.gz /workdir/mbb262/results/goodman_permuted
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/gwa_results_goodman_panel_kremling_maffilter/kremling_permuted/processed_results/*.gz /workdir/mbb262/results/kremling_filtered_permuted/processed_results

# Unzip files
cd /workdir/mbb262/results/nam_permuted
cd /workdir/mbb262/results/goodman_permuted
cd /workdir/mbb262/results/kremling_filtered_permuted/processed_results
for FILE in *.gz
do
    bgzip -d --threads 60 ${FILE}
done

# Split metabolite traits and field traits
cd /workdir/mbb262/results/goodman_permuted
mv *Zhou_2019* /workdir/mbb262/results/goodman_metabolite_permuted/processed_results
mv ./* /workdir/mbb262/results/goodman_field_permuted/processed_results


# Do count for NAM
cd /workdir/mbb262/results/nam_permuted

# Do on a single test file
# /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
#     -Xmx200g  \
#     -debug /workdir/mbb262/debug.txt \
#     -CountAssociationsPlugin \
#     -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
#     -gwasResults chrom_8_Benson_2015_permutedSNPs_reformatted_zack.csv \
#     -outputCountFile /workdir/mbb262/test_count.csv \
#     -endPlugin
# vim /workdir/mbb262/debug.txt
# vim /workdir/mbb262/test_count.csv
# cd /workdir/mbb262/results/nam_all
for FILE in *.csv
do
    echo "Start analyzing trait file: ${FILE}"
    /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/output_counts/nam_permuted_count/debug.txt \
    -Xmx200g \
    -CountAssociationsPlugin \
    -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
    -gwasResults $FILE \
    -outputCountFile /workdir/mbb262/output_counts/nam_permuted_count/${FILE}_outputCountSNP.txt \
    -endPlugin
done

# Check if the order of traits is the same
for f in ./*outputCountSNP.txt; do
    
    if [ -z "$first_intervals" ]; then
        first_intervals=$( awk  '{ print $1 }' $f )
    else
        intervals=$( awk  '{ print $1 }' $f )
        
        if [ "$first_intervals" != "$intervals" ]; then
            echo "File $f does not match interval order!"
        fi
    fi
done


# Do count for goodman panel field traits - permuted
cd /workdir/mbb262/results/goodman_field_permuted/processed_results
for FILE in *.csv
do
    echo "Start analyzing trait file: ${FILE}"
    /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/output_counts/goodman_permuted_count/debug.txt \
    -Xmx200g \
    -CountAssociationsPlugin \
    -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
    -gwasResults $FILE \
    -outputCountFile /workdir/mbb262/output_counts/goodman_permuted_count/${FILE}_outputCountSNP.txt \
    -endPlugin
done

# Do count for goodman panel metabolite traits - non-parallel version
# cd /workdir/mbb262/results/goodman_metabolite_permuted/processed_results
# for FILE in *.csv
# do
#     echo "Start analyzing trait file: ${FILE}"
#     /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
#     -debug /workdir/mbb262/output_counts/goodman_metabolite_permuted_count/debug.txt \
#     -Xmx200g \
#     -CountAssociationsPlugin \
#     -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
#     -gwasResults $FILE \
#     -outputCountFile /workdir/mbb262/output_counts/goodman_metabolite_permuted_count/${FILE}_outputCountSNP.txt \
#     -endPlugin
# done

# Goodman metabolite parallel version, running on em machine
find /workdir/mbb262/results/goodman_metabolite_permuted/processed_results/*.csv -type f > metabolite_filt_files.list
/programs/parallel/bin/parallel -j 23 "/home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl -debug /workdir/mbb262/output_counts/goodman_metabolite_permuted_count/debug.txt -Xmx200g -CountAssociationsPlugin -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv -gwasResults {} -outputCountFile /workdir/mbb262/output_counts/goodman_metabolite_permuted_count/outputCountSNP_{/.}.txt -endPlugin" :::: /workdir/mbb262/results/goodman_metabolite_permuted/processed_results/metabolite_filt_files.list



# Do count for goodman panel expression permuted
cd /workdir/mbb262/results/kremling_filtered_permuted/processed_results/

# # Do on a single test trait
# /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
#     -debug /workdir/mbb262/debug.txt \
#     -Xmx200g \
#     -CountAssociationsPlugin \
#     -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv \
#     -gwasResults chrom_9_fast_assoc_results_permutedSNPs_GRoot_Kremling_2018_permutedSNPs_reformatted_zack.csv \
#     -outputCountFile /workdir/mbb262/temp_output.txt \
#     -endPlugin
# vim /workdir/mbb262/debug.txt
# vim /workdir/mbb262/temp_output.txt

# Goodman panel expression permuted as a parallel command --> works much faster!
# find all files in directory ending with csv, save to file
find /workdir/mbb262/results/kremling_filtered_permuted/processed_results/*.csv -type f > kremling_filt_files.list

# run count association plugin in parallel
# Ran on a lm machine
/programs/parallel/bin/parallel -j 15 "/home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl -debug /workdir/mbb262/output_counts/kremling_permuted_count/debug.txt -Xmx200g -CountAssociationsPlugin -intervals /workdir/mbb262/snp_intervals_b73_v4.49.csv -gwasResults {} -outputCountFile /workdir/mbb262/output_counts/kremling_permuted_count/outputCountSNP_{/.}.txt -endPlugin" :::: /workdir/mbb262/results/kremling_filtered_permuted/processed_results/kremling_filt_files.list







