#!/usr/bin/env bash

set \
  -o errexit \
  -o nounset \
  -o pipefail

# ---------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2022-01-04 
# Updated... 2022-03-01
#
# Description 
# Count the total number of significant GWAS hits for filtered data
# ---------------------------------------------------------------

# NAM physiological 
# 4798539 total lines
# 160 files
cd /data1/users/mbb262/results/pleiotropy/gwa_rtassel_all_nam_traits/filtered_nam_triplet_genic_intergenic_top1percent_peaks
find . -name '*.csv' | xargs wc -l
find . -type f | wc -l

# Goodman physiological and metabolite
# 92,837,596 total lines
# 180 files
cd 
gunzip *.gz
find . -name '*.csv' | xargs wc -l
find . -type f | wc -l
gzip *.csv

# Goodman expression
# 526412922 total lines
# 70 files
cd /data1/users/mbb262/results/pleiotropy/gwa_results_goodman_panel_kremling_maffilter/filtered_goodman_kremling_triplet_genic_intergenic_top1percent_peaks
gunzip *.gz
find . -name '*.csv' | xargs wc -l
find . -type f | wc -l
gzip *.csv
