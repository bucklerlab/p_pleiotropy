# ---------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2022-12-22
# Updated... 2022-12-22

# Description 
# Gather only unique traits, filter count table, sum across traits
# and compare permuted and un-permuted results
# Need to gather unique traits for NAM and Goodman Field traits
# Split by tissue for expression traits
# DO nothing for the metabolite traits
#
# Run on a lm machine
#
# Calculate by interval
# ---------------------------------------------------------------

# ----------------------------
# Load in helpful packages
# -----------------------------

library(dplyr)
library(data.table)

# Set global parameters
n_threads <- 60


# ------------------------------------------------------------------------------
#                      Gather Data
# ------------------------------------------------------------------------------

# -----------------------------
# Gather data from cbsu blfs1
# Data: Count of traits within intervals
# counts calculated with Zack's tassel plugin
# -----------------------------

# Count Matrices
# mkdir -p /workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/interval_data/aggregated_counts/aggregated_by_pop_or_trait/* /workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait


# Unzip files
# for FILE in *.gz
# do
# bgzip -d --threads 60 ${FILE}
# done


# ------------------------------------------------------------------------------------------
#                      Gather Data
# ------------------------------------------------------------------------------------------

# Different datasets
# - Nam observed physiological (nop)
# - Nam permuted physiological (npp)
# - Goodman observed physiological (gop)
# - Goodman permuted physiological (gpp)
# - Goodman observed metabolite (gom)
# - Goodman permuted metabolite (gpm)
# - Goodman observed expression (goe)
# - Goodman permuted expression (gpe)


# ------------------------------------------------------------------------------ 
# Format data
# ------------------------------------------------------------------------------

# Load in file to subset out unique traits
unique_traits <- read.csv("/home/mbb262/git_projects/pleiotropy/data/unique_field_traits.csv", header = TRUE)
unique_traits$trait <- gsub("_nam", "", unique_traits$trait)
unique_traits$trait <- gsub("_goodman", "", unique_traits$trait)

# remove the traits not in GWA results (they're not even in initial phenotype files)
unique_traits <- unique_traits %>% filter(!trait %in% c("A_Absolute_PBAA_kernel_aminoacid_blup_Shrestha_2022", 
                                                        "A_div_Total_Relative_composition_PBAA_kernel_aminoacid_blup_Shrestha_2022",
                                                        "G_Absolute_PBAA_kernel_aminoacid_blup_Shrestha_2022", 
                                                        "G_div_Total_Relative_composition_PBAA_kernel_aminoacid_blup_Shrestha_2022"))

# Remove duplicated traits
unique_traits <- unique_traits %>% filter(keep == 1)

# Load trait categories (8 different categories)
# from: https://docs.google.com/spreadsheets/d/1ORBojmruDj4gyZtNHEPYmdGIJn8ZcnroM9PTkDOMVsc/edit#gid=1640556379
trait_grouping <- read.csv("/workdir/mbb262/main_phenotypes_melted.csv") %>% 
  filter(trait_type == "physiological") %>% 
  select(trait_category, traits, population)

# Format trait names by population
fixNamesNAM <- trait_grouping %>% filter(population == "NAM") 
fixNamesNAM$traits <- gsub(";", ".", fixNamesNAM$traits)
fixNamesNAM$traits <- gsub("_nam$", "", fixNamesNAM$traits)
fixNamesGoodman <- trait_grouping %>% filter(population == "Goodman_Association")
fixNamesGoodman$traits <- gsub(";", "_", fixNamesGoodman$traits)
trait_grouping <- rbind(fixNamesNAM, fixNamesGoodman) # recombine formatted names

# Due to mispelling traits in the initial trait files (before association mapping)
# around 13 traits are mispelled compared to the metadata file. Here I try to correct that
# so that these mispelled traits are counted later in the pipeline
# Things that need to be replaced
trait_grouping$traits <- gsub("alpha_carotene_div_zeinanthin_BLUP_Owens_2014", "alpha_carotene_div_zeinoxanthin_BLUP_Owens_2014", trait_grouping$traits)
trait_grouping$traits <- gsub("beta_carotene_div_beta_cryptanthin_BLUP_Owens_2014", "beta_carotene_div_beta_cryptoxanthin_BLUP_Owens_2014", trait_grouping$traits)
trait_grouping$traits <- gsub("beta_cryptanthin_BLUP_Owens_2014", "beta_cryptoxanthin_BLUP_Owens_2014",  trait_grouping$traits)
trait_grouping$traits <- gsub( "beta_cryptanthin_div_zeanthin_BLUP_Owens_2014", "beta_cryptoxanthin_div_zeaxanthin_BLUP_Owens_2014", trait_grouping$traits)
trait_grouping$traits <- gsub("betaanthophylls_div_alphaanthophylls_BLUP_Owens_2014", "beta_xanthophylls_div_alpha_xanthophylls_BLUP_Owens_2014",trait_grouping$traits)
trait_grouping$traits <- gsub("betacarotene_div_startparen_beta_cryptanthin_plus_zeanthin_endparen_BLUP_Owens_2014","betacarotene_div_startparen_beta_cryptoxanthin_plus_zeaxanthin_endparen_BLUP_Owens_2014",  trait_grouping$traits)
trait_grouping$traits <- gsub( "total_alphaanthophylls_BLUP_Owens_2014","total_alpha_xanthophylls_BLUP_Owens_2014", trait_grouping$traits)
trait_grouping$traits <- gsub( "total_betaanthophylls_BLUP_Owens_2014","total_beta_xanthophylls_BLUP_Owens_2014", trait_grouping$traits)
trait_grouping$traits <- gsub( "totalcarotenes_div_totaanthophylls_BLUP_Owens_2014","totalcarotenes_div_totalxanthophylls_BLUP_Owens_2014", trait_grouping$traits)
trait_grouping$traits <- gsub( "zeanth_in_raw_Harjes_2008","zeaxanth_in_raw_Harjes_2008", trait_grouping$traits)
trait_grouping$traits <- gsub("zeanthin_BLUP_Owens_2014","zeaxanthin_BLUP_Owens_2014",  trait_grouping$traits)
trait_grouping$traits <- gsub( "zeinanthin_BLUP_Owens_2014","zeinoxanthin_BLUP_Owens_2014", trait_grouping$traits)
trait_grouping$traits <- gsub( "zeinanthin_div_lutein_BLUP_Owens_2014","zeinoxanthin_div_lutein_BLUP_Owens_2014", trait_grouping$traits)

# Only take unique traits
trait_grouping <- trait_grouping %>% filter(traits %in% unique_traits$trait)

# Gather unique traits to iterate through - NAM
grouping_NAM <- trait_grouping %>%  
  filter(population == "NAM")
unique_trait_categories_NAM <- unique(grouping_NAM$trait_category)

# remove expression and metabolite data, filter to only Goodman
grouping_GAP <- trait_grouping %>% 
  filter(population == "Goodman_Association")
unique_trait_categories_GAP <- unique(grouping_GAP$trait_category)


# Check for inconsistencies between metadata files
# There should be no rows with unassigned traits into trait categories
# There arent! Woo hoo!
unique_traits_sub <- unique_traits %>% filter(pop == "Goodman Field")
lala <- merge(grouping_GAP, unique_traits_sub, by.x = "traits", by.y = "trait", all = TRUE)
unique_traits_sub <- unique_traits %>% filter(pop == "NAM Field")
lala <- merge(grouping_NAM, unique_traits_sub, by.x = "traits", by.y = "trait", all = TRUE)


# ------------------------------------------------------------------------------------------
#                        Non-Permuted data counts 
# ------------------------------------------------------------------------------------------

# Datatbale function to replace NAs with 0's
replace_nas <- function(DT) {
  for (j in seq_len(ncol(DT)))
    set(DT,which(is.na(DT[[j]])),j,0)
  
  return(DT)
}

# NOTE: script 15_aggregate_gwa_counts leaves NAs when there are no GWA hits in any intervals
# we need to replace those NAs with 0's (e.g. there are only 3 sig. snps for a trait, the focal
# interval will have a count of 3 but all other intervals will have NA values)

# Start loading in data
# data_path <- "/Volumes/merrittData1/pleiotropy/aggregated_counts/aggregated_by_pop_or_trait/"
data_path <- "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/"

# - Nam observed physiological (nop)
nop <- data.table::fread(paste0(data_path, "nam_filtered_all_traits.txt"))
nop[is.na(nop)] <- 0
colnames(nop) <- gsub("_nam", "", colnames(nop)) # remove the two columns that end with _nam
nam_traits <- unique_traits %>% filter(pop == "NAM Field" & keep == 1) %>% select(trait)
nop <- nop %>% select(nam_traits$trait)

# - Goodman observed physiological (gop)
gop <- data.table::fread(paste0(data_path, "goodman_filtered_physiological.txt"))
colnames(gop) <- gsub("_goodman", "", colnames(gop)) # remove the two columns that end with _goodman
gop[is.na(gop)] <- 0
good_traits <- unique_traits %>% filter(pop == "Goodman Field" & keep == 1) %>% select(trait)
gop <- gop %>% select(good_traits$trait)


# - Goodman observed metabolite (gom)
gom <- data.table::fread(paste0(data_path, "goodman_filtered_metabolite.txt"), nThread = n_threads)
gom[is.na(gom)] <- 0


# - Goodman observed expression (goe)
goe <- data.table::fread(paste0(data_path, "goodman_filtered_expression.txt"), nThread = n_threads)
goe <- cbind(goe[,1], replace_nas(goe[,-1]))
expression_names <- colnames(goe) # subset out column names from this expression data

# Collect unique tissue names to iterate through
exp_tissues <- gsub(".*_", "", colnames(goe)) %>% unique()
exp_tissues <- exp_tissues[-1]


# ------------------------------------------------------------------------------
#                                       Expression tangent
# ------------------------------------------------------------------------------

# For some reason, mapping the same expression data led to different numbers of columns, 
# due to some permutations having no significant hits genome wide and having nothing in the 
# fast association output. 
# This next step gathers column names, intersect them all to get shared names (i.e. same columns)
# so that pleiotropy is calculated on all the same columns. This isn't totally necessary but it only drops a few columns
gather_col_names <- function(file_dir, file_name){
  # Find files with matching string in file name
  find_files <- list.files(file_dir, pattern = file_name, full.names = TRUE)
  
  # Load all into nested list
  perm_list <- lapply(find_files, data.table::fread, nThread = n_threads)
  
  # Gather column names from each list
  gather_names <- lapply(perm_list, colnames)
  
  # remove what permutation part of column names
  for (i in 1:length(gather_names)){
    gather_names[[i]] <- gsub("_permutation_[0-9]", "",gather_names[[i]])
  }
  
  # Intersect all names across lists and return
  df <- Reduce(intersect, gather_names)
  return(df)
}

# Gather goodman expression names shared among all permutations and 
# the biological (non-permuted set) --> length = 116,367 (before)
perm_dir <- "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait"
gpe_names <- gather_col_names(file_dir = perm_dir, file_name = "goodman_expression_permutation*")
expression_intersection <- intersect(gpe_names, expression_names)

# number of expression traits we're working with that match across everything:116382
length(expression_intersection) 


# ------------------------------------------------------------------------------------------
# Functions to calculate pleiotropy metric for non-permuted data
# ------------------------------------------------------------------------------------------

# Function makes pleiotropy counts by expression tissue
# Does not return the names of significant expression phenotypes by interval
calc_pleiotropy_expression <- function(aggregated_count_df, data_id_to_paste){
  
  # Grab interval ID
  unique_count <- aggregated_count_df[,1]
  
  # Iterate through different tissues, make tissue specific count
  for (i in exp_tissues){
    
    # Find columns with that specific tissue
    aggregated_count_df_sub <- data.frame(aggregated_count_df)
    lala <- grepl(i, colnames(aggregated_count_df_sub))
    aggregated_count_df_sub <- aggregated_count_df_sub[,lala]
    
    # If wanted significant traits by interval
    # # Gather names of traits with associations by SNP
    # traitsMapped <- data.frame(apply(aggregated_count_df_sub, 1, function(x) paste(colnames(aggregated_count_df_sub)[ which(x>0) ], collapse = ",")))
    # colnames(traitsMapped) <- paste0("traitsMapped_", i)
    
    # For each tissue, make count by interval
    print(dim(aggregated_count_df_sub))
    temp_count <- rowSums(aggregated_count_df_sub != 0) %>% data.frame()
    colnames(temp_count)[1] <- i
    
    # Combine with df outside of loop
    # Add this to cbind statement if wanted names of sig. expression traits by interval: traitsMapped
    unique_count <- merge(unique_count, 
                          cbind(aggregated_count_df[,1], temp_count),
                          by = "Interval")
    print(head(unique_count))
  }
  
  # Return function
  return(unique_count)
}

# Function makes pleiotropy counts for all mass features
# Does not return the names of signifcant mass features by interval
calc_pleiotropy_massFeatures <- function(aggregated_count_df, data_id_to_paste){
  
  # # Gather names of traits with associations by interval
  sub_aggregated_count_df <- aggregated_count_df[,-1]
  # traitsMapped <- data.frame(apply(sub_aggregated_count_df, 1, function(x) paste(colnames(sub_aggregated_count_df)[ which(x>0) ], collapse = ",")))
  # colnames(traitsMapped) <- "traitsMapped"
  
  # Count of unique traits
  unique_count <- rowSums(sub_aggregated_count_df != 0)
  
  # Rejoin medians with interval IDs
  # Add this to cbind statement if wanted names of sig. expression traits by interval: traitsMapped
  interval_metrics <- cbind(aggregated_count_df[,1], data.table(unique_count))
  
  # Change column names
  colnames(interval_metrics)[2] <- paste0(data_id_to_paste, "_uniqueCount")
  
  # Return function
  return(interval_metrics)
}

# Function to calculate the number of unique traits mapping to each interval
calc_pleiotropy_field <- function(aggregated_count_df, data_id_to_paste, pop){
  # Gather data outside of loop
  outside <- c()
  outside <- data.frame(aggregated_count_df$Interval)
  colnames(outside) <- "Interval"
  
  # select correct population to iterate through
  if (pop == "NAM"){
    unique_trait_categories <- unique_trait_categories_NAM
    grouping <- grouping_NAM
  } else if (pop == "GAP") {
    unique_trait_categories <- unique_trait_categories_GAP
    grouping <- grouping_GAP
  }
  
  # Loop through unique traits and make counts by trait category
  for (i in 1:length(unique_trait_categories)) {
    # Print the trait category
    message(paste0("I am counting trait category: ", unique_trait_categories[i]))
    
    # Select traits within this category in the cross-reference table
    category_sub <- grouping %>% 
      filter(trait_category == unique_trait_categories[i]) %>% 
      select(traits)
    
    # Subset the count df by columns in a particular trait category
    sub_test <- aggregated_count_df %>% select(category_sub$traits)
    
    # Return column names if trait associates in that interval/SNP
    traitsMapped <- data.frame(apply(sub_test, 1, function(x) paste(colnames(sub_test)[ which(x>0) ], collapse = ",")))
    
    # Count sum of unique traits in a particular trait category
    unique_count <- rowSums(sub_test != 0)
    
    # Gather and format data
    together <- cbind(aggregated_count_df[,1], data.frame(unique_count), traitsMapped)
    colnames(together) <- c("Interval", 
                            paste0("unique_count", unique_trait_categories[i]), 
                            paste0("traitsMapped_", unique_trait_categories[i]))
    
    # Combine outside of loop
    outside <- merge(outside, together, by = "Interval")
    
  }
  
  # Get count across all traits and categories
  # Gather names of traits with associations by SNP
  sub_aggregated_count_df <- aggregated_count_df[,-1]
  traitsMapped <- data.frame(apply(sub_aggregated_count_df, 1, function(x) paste(colnames(sub_aggregated_count_df)[ which(x>0) ], collapse = ",")))
  colnames(traitsMapped) <- "traitsMapped"
  
  # Count of unique traits
  unique_count <- rowSums(sub_aggregated_count_df != 0)
  
  # Rejoin medians with interval IDs
  interval_metrics <- cbind(aggregated_count_df[,1], data.table(unique_count), traitsMapped)
  
  # Change column names
  colnames(interval_metrics)[2] <- paste0(data_id_to_paste, "_uniqueCount")
  
  # Merge with individual category counts
  interval_metrics <- merge(interval_metrics, outside, by = "Interval")
  
  # Return results of function
  return(interval_metrics)
}

# Permuted field trait count
count_permuted_traits_field <- function(file_dir, file_name, pop){
  # Find files with matching string in file name
  find_files <- list.files(file_dir, pattern = file_name, full.names = TRUE)
  
  # Load all into nested list
  perm_list <- lapply(find_files, data.table::fread, nThread = n_threads)
  
  # Load in data by permutation into elements of a list
  # Subset by trait name to unique traits
  # gather population specific trait type file to iterate through
  if (pop == "Goodman"){
    perm_list <- lapply(perm_list, function(x){colnames(x) <- gsub("_goodman_permutation_[0-9]{1,2}", "", colnames(x)); x})
    perm_list <- lapply(perm_list, function(x) x %>% select(any_of(good_traits$trait)))
    unique_trait_categories <- unique_trait_categories_GAP
    grouping <- grouping_GAP
  } else if (pop == "NAM"){
    perm_list <- lapply(perm_list, function(x) {colnames(x) <- gsub("_nam_permutation_[0-9]{1,2}", "", colnames(x)); x})
    perm_list <- lapply(perm_list, function(x){x %>% select(nam_traits$trait)})
    unique_trait_categories <- unique_trait_categories_NAM
    grouping <- grouping_NAM
  } 
  
  # Keep data outside of loop
  outside_df <- data.frame(Interval = perm_list[[1]]$Interval)
  
  # Loop through unique traits and make counts by trait category
  for (i in 1:length(unique_trait_categories)) {
    # Print the trait category
    message(paste0("I am counting trait category: ", unique_trait_categories[i]))
    
    # Select traits within this category in the cross-reference table
    category_sub <- grouping %>% 
      filter(trait_category == unique_trait_categories[i]) %>% 
      select(traits)
    
    # Subset the count df by columns in a particular trait category
    sub_test <- lapply(perm_list, function(x) x %>% select(category_sub$traits))
    
    # Return column names if trait associates in that interval/SNP
    traitsMappedEachCategory <- lapply(sub_test, function(x) data.frame(apply(x, 1, function(k) paste(colnames(x)[ which(k>0) ], collapse = ","))))
    
    # Count sum of unique traits in a particular trait category
    unique_count <- lapply(sub_test, function(x) data.frame(rowSums(x != 0)))
    
    # Combine three different lists
    sub_perm_list_names <- lapply(perm_list, function(x) data.frame(x[,1])) # subset just names
    together <- Map(cbind, sub_perm_list_names, unique_count, traitsMappedEachCategory) # combine everything
    
    # Change column names within these files
    newColNames <- c("Interval", 
                     paste0("unique_count", unique_trait_categories[i]), 
                     paste0("traitsMapped_", unique_trait_categories[i]))
    together <- lapply(together, setNames, newColNames)
    
    # Change colnames names to add permutation info 
    for (i in 1:length(find_files)){
      colnames(together[[i]])[2:ncol(together[[i]])] <- paste0("perm_", i, "_", colnames(together[[i]])[2:ncol(together[[i]])])
    }
    
    # Turn into dataframe, merge with the outside collector
    df <- Reduce(function(x, y) merge(x, y, by = "Interval"), together)
    
    # Merge with outside df
    outside_df <- merge(outside_df, df, by = "Interval")
  }
  
  # Count number of unique traits within each permutation round
  count_perm <- lapply(perm_list, function(x) cbind(x[,1], rowSums(x[,-1] != 0)))
  print(lapply(perm_list, dim))
  
  # Change colnames names to add permutation info
  for (i in 1:length(find_files)){
    colnames(count_perm[[i]])[2] <- paste0("perm_", i)
  }
  
  # Turn into dataframe
  df_crossTraits <- Reduce(function(x, y) merge(x, y, by = "Interval"), count_perm)
  
  # Merge with other df
  outside_df <- merge(df_crossTraits, outside_df, by = "Interval")
  
  # Return
  return(outside_df)
}

# Permuted metabolite trait count
# NOTE: replaced all NAs with 0's in prior 15_aggregate_gwa_counts.R script
# Function to make counts for each permutation
count_permuted_traits_massFeatures <- function(file_dir, file_name){
  # Find files with matching string in file name
  find_files <- list.files(file_dir, pattern = file_name, full.names = TRUE)
  
  # Load all into nested list
  perm_list <- lapply(find_files, data.table::fread, nThread = n_threads)
  
  # Count number of unique traits within each permutation round
  count_perm <- lapply(perm_list, function(x) cbind(x[,1], rowSums(x[,-1] != 0)))
  print(lapply(perm_list, dim))
  
  # Change colnames names to add permutation info
  for (i in 1:length(find_files)){
    colnames(count_perm[[i]])[2] <- paste0("perm_", i)
  }
  
  # Turn into dataframe
  df <- Reduce(function(x, y) merge(x, y, by = "Interval"), count_perm)
  
  # Return
  return(df)
}


# ------------------------------------------------------------------------------
# Use the functions on observed data
# ------------------------------------------------------------------------------

# Nam observed physiological (nop)
nop_pleiotropy <- calc_pleiotropy_field(nop, "nam_filtered_all", pop = "NAM")

# Check if individual trait counts add up to overall count
temp <- nop_pleiotropy[,c(1,2,4,6,8,10,12,14,16,18)]
temp$sumsss <- rowSums(temp[,-c(1:2)])
temp$logic <- c(temp[,2] == temp$sumsss)
temp$diff <- temp[,2] - temp$sumsss
summary(temp[,2] == temp$sumsss) # Should be all true AND THEY ARE!!


# Goodman observed physiological (gop)
gop_pleiotropy <- calc_pleiotropy_field(gop, "goodman_filtered_physiological", pop = "GAP")

# Check if individual trait counts add up to overall count
temp <- gop_pleiotropy[,c(1,2,4,6,8,10,12,14,16,18)]
temp$sumsss <- rowSums(temp[,-c(1:2)])
temp$logic <- c(temp[,2] == temp$sumsss)
temp$diff <- temp[,2] - temp$sumsss
summary(temp[,2] == temp$sumsss) # Should be all true AND THEY ARE!


# Goodman observed metabolite (gom)
gom_pleiotropy <- calc_pleiotropy_massFeatures(gom, "goodman_filtered_metabolite")

# Goodman observed expression (goe) --> CRASHES ON A MM MACHINE, fine on LM machine
dim(goe)
goe <- goe[,..expression_intersection] # subset to shared columns below
dim(goe) # Should be 116,382
goe_pleiotropy <- calc_pleiotropy_expression(goe, "goodman_filtered_expression")


# temporarily write files for insurance purposes
data.table::fwrite(goe_pleiotropy, "/workdir/mbb262/observed_pleiotropy.csv", nThread = n_threads)
# goe_pleiotropy <- data.table::fread("/workdir/mbb262/observed_pleiotropy.csv")


# Add on identifier for population
colnames(nop_pleiotropy)[3:ncol(nop_pleiotropy)] <- paste0("nfo_", colnames(nop_pleiotropy)[3:ncol(nop_pleiotropy)]) # nam field observed
colnames(gop_pleiotropy)[3:ncol(gop_pleiotropy)] <- paste0("gfo_", colnames(gop_pleiotropy)[3:ncol(gop_pleiotropy)]) # goodman field observed


# ------------------------------------------------------------------------------
# Use the functions on the PERMUTED data
# ------------------------------------------------------------------------------

# Directory where all files are
perm_dir <- "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/"

# - Nam permuted physiological (npp)
npp <- count_permuted_traits_field(file_dir = perm_dir, 
                           file_name = "nam_physiological_permutation*", 
                           pop = "NAM")

# Check if the function is working and giving the expected counts with perm 1 data
temp <- npp[,c("Interval", "perm_1", "perm_1_unique_countFlowering", "perm_1_unique_countDisease", "perm_1_unique_countLeaf", "perm_1_unique_countKernel", "perm_1_unique_countVegetative", "perm_1_unique_countHeight", "perm_1_unique_countEar", "perm_1_unique_countTassel")]
temp$sumsss <- rowSums(temp[,-c(1:2)])
temp$logic <- c(temp$perm_1 == temp$sumsss)
temp$diff <- temp$perm_1 - temp$sumsss
summary(temp$perm_1 == temp$sumsss) # Should be all true AND THEY ARE!!!!


# - Goodman permuted physiological (gpp)
gpp <- count_permuted_traits_field(file_dir = perm_dir, 
                           file_name = "goodman_physiological_permutation*", 
                           pop = "Goodman")

# Check if the function is working with perm 1 data
temp <- gpp[,c("Interval", "perm_1", "perm_1_unique_countFlowering", "perm_1_unique_countDisease", "perm_1_unique_countLeaf", "perm_1_unique_countKernel", "perm_1_unique_countVegetative", "perm_1_unique_countHeight", "perm_1_unique_countEar", "perm_1_unique_countTassel")]
temp$sumsss <- rowSums(temp[,-c(1:2)])
temp$logic <- c(temp$perm_1 == temp$sumsss)
temp$diff <- temp$perm_1 - temp$sumsss
summary(temp$perm_1 == temp$sumsss) # Should be all true AND THEY ARE!!!!


# - Goodman permuted metabolite (gpm)
gpm <- count_permuted_traits_massFeatures(file_dir = perm_dir, 
                                          file_name = "goodman_metabolite_permutation*")

# - Goodman permuted expression (gpe) --> do on a large memory machine
# Find files with matching string in file name
find_files <- list.files(perm_dir, pattern = "goodman_expression_permutation*", full.names = TRUE)

# Load all files separately
perm_1 <- data.table::fread(find_files[1], nThread = n_threads)
perm_2 <- data.table::fread(find_files[2], nThread = n_threads)
perm_3 <- data.table::fread(find_files[3], nThread = n_threads)
perm_4 <- data.table::fread(find_files[4], nThread = n_threads)
perm_5 <- data.table::fread(find_files[5], nThread = n_threads)

# Make a place to collect all tissue and permutation counts
tissue_counts_perm1 <- perm_1[,1] %>% data.frame()
tissue_counts_perm2 <- perm_2[,1] %>% data.frame()
tissue_counts_perm3 <- perm_3[,1] %>% data.frame()
tissue_counts_perm4 <- perm_4[,1] %>% data.frame()
tissue_counts_perm5 <- perm_5[,1] %>% data.frame()

# Tisses to iterate though
exp_tissues <- c("GRoot", "GShoot", "Kern", "L3Base", "L3Tip", "LMAD", "LMAN")

countExpressionHits <- function(df, perm_round, unique_count_file){
  # turn into a dataframe
  df <- data.frame(df)
  
  for (i in 1:length(exp_tissues)){
    # Print dimensions + tissue before subsetting
    print(exp_tissues[i])
    print(dim(df))
    
    # get indexes in each list of things that should be kept/discarded.
    lala <- grepl(exp_tissues[i], colnames(df))
    lala[1] <- TRUE # keep the first interval column
    
    # subset dataframe based on indexes
    df_sub <- df[,lala]
    print(dim(df_sub))
    
    # Take sum of unique traits across rows (for a single tissue)
    df_count <- data.frame(df_sub[,1], rowSums(df_sub[,-1] != 0))
    colnames(df_count) <- c("Interval", paste0(exp_tissues[i], "_perm_", perm_round))
    
    # Combine counts for each tissue to their permutation round file
    unique_count_file <- merge(unique_count_file, df_count)
    print(head(unique_count_file)) # check
  }
  # return
  return(unique_count_file)
}

# Do counts for each permutation
counts_perm_1 <- countExpressionHits(df = perm_1, perm_round = 1, unique_count_file = tissue_counts_perm1)
data.table::fwrite(counts_perm_1, "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_1.csv", nThread = n_threads)

counts_perm_2 <- countExpressionHits(df = perm_2, perm_round = 2, unique_count_file = tissue_counts_perm2)
data.table::fwrite(counts_perm_2, "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_2.csv", nThread = n_threads)

counts_perm_3 <- countExpressionHits(df = perm_3, perm_round = 3, unique_count_file = tissue_counts_perm3)
data.table::fwrite(counts_perm_3, "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_3.csv", nThread = n_threads)

counts_perm_4 <- countExpressionHits(df = perm_4, perm_round = 4, unique_count_file = tissue_counts_perm4)
data.table::fwrite(counts_perm_4, "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_4.csv", nThread = n_threads)

counts_perm_5 <- countExpressionHits(df = perm_5, perm_round = 5, unique_count_file = tissue_counts_perm5)
data.table::fwrite(counts_perm_5, "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_5.csv", nThread = n_threads)

# # Alternately, if ran previously, load them all into R
# counts_perm_1 <- data.table::fread("/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_1.csv")
# counts_perm_2 <- data.table::fread("/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_2.csv")
# counts_perm_3 <- data.table::fread("/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_3.csv")
# counts_perm_4 <- data.table::fread("/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_4.csv")
# counts_perm_5 <- data.table::fread("/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/exp_counts_perm_5.csv")

# Combine all expression counts into a single file
gpe <- Reduce(function(x, y) merge(x, y, by = "Interval"), 
              list(counts_perm_1, counts_perm_2, counts_perm_3, counts_perm_4, counts_perm_5))


# append on names so merging isn't a nightmare -----------------------------------------------
colnames(npp)[2:ncol(npp)] <- paste0("npp_unique_", colnames(npp)[2:ncol(npp)])
colnames(gpp)[2:ncol(gpp)] <- paste0("gpp_unique_", colnames(gpp)[2:ncol(gpp)])
colnames(gpm)[2:ncol(gpm)] <- paste0("gpm_", colnames(gpm)[2:ncol(gpm)])
colnames(gpe)[2:ncol(gpe)] <- paste0("gpe_unique_", colnames(gpe)[2:ncol(gpe)])


# ------------------------------------------------------------------------------------------
#     Unit tests for permuted and non-permuted data
# ------------------------------------------------------------------------------------------

# Check dimensions between filterd and permuted sets (permuted dimensions printed in above function)
dim(nop) # nam 75490    81
dim(gop) # goodman physiological 75490   168
dim(gom) # goodman metabolite 75490  3874
dim(goe) # goodman expression --> final # of traits 75490 116382

dim(npp) #75490   171
dim(gpp) #75490   171
dim(gpm) # 75490    11
dim(gpe) #75490    36


# They all match in dimensions!!!!! Woo!


# ------------------------------------------------------------------------------------------
#       Combine and export all counts 
# ------------------------------------------------------------------------------------------

# Combine all into single df
all_pleiotropy <- Reduce(function(x, y) merge(x, y, by = "Interval"), 
                         list(nop_pleiotropy, npp, 
                              gop_pleiotropy, gpp,
                              gom_pleiotropy, gpm, 
                              goe_pleiotropy, gpe))
dim(all_pleiotropy)
colnames(all_pleiotropy)

# Save to file
data.table::fwrite(all_pleiotropy, 
                   file = "/workdir/mbb262/aggregated_counts/aggregated_by_pop_or_trait/goodman_nam_allTraitTypes_gwaCounts_unique_withTraitCategories.txt",
                   nThread = n_threads)


