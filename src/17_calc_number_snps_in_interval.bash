# ---------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2021-09-29 
#
# Description 
# - Calculate number of SNPs per interval (not # of GWA snps)
#   use bedtools
# ---------------------------------------------------------------

# R code to run this and create the regions_file.bed
# all_interval_data <- fread("/workdir/mbb262/genic_intergenic_intervals_b73_v4.49.csv")
# reg <- all_interval_data %>% select(seqnames, start, end)
# data.table::fwrite(reg, "/workdir/mbb262/regions_file.bed", col.names = FALSE, sep = "\t")


# ----------------------------------------------------
# ALL SNPs (used for both permuted and non-perumted sets)
# ----------------------------------------------------

# get data from blfs1: 
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/genotypes/goodman282/*_imputed_goodman282.vcf.gz /workdir/mbb262/genotypes/goodman
scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/genotypes/nam/imputed/beagle_intersect_nam/*vcf.gz /workdir/mbb262/genotypes/nam

# unzip all files in directory then bgzip them
gzip -d *.vcf.gz
for FILE in *.vcf
do
    bgzip --threads 50 ${FILE}
done


# Run bedtools function - goodman panel
for CHROM in {1..10}
do
  echo "Start analyzing chromosome ${CHROM}"

  bedtools coverage \
  -a /workdir/mbb262/regions_file.bed \
  -b /workdir/mbb262/genotypes/goodman/hmp321_282_agpv4_merged_chr${CHROM}_imputed_goodman282.vcf.gz \
  -counts > /workdir/mbb262/interval_data/snp_counts_all/snp_count_hmp321_282_agpv4_chr_${CHROM}.txt

  echo "End analyzing chromosome ${CHROM}"
done

# run bedtools function - NAM, crashed a machine once, be careful
for CHROM in {2..10}
do
  echo "Start analyzing chromosome ${CHROM}"

  bedtools coverage \
  -a /workdir/mbb262/regions_file.bed \
  -b /workdir/mbb262/genotypes/nam/nam_ibm_imputed_intersectMAFFilter_chr${CHROM}.vcf.gz \
  -counts > /workdir/mbb262/interval_data/snp_counts_all/snp_count_nam_ibm_imputed_chr_${CHROM}.txt
  
  echo "End analyzing chromosome ${CHROM}"
done

# Run chrom 1 on an em machine
bedtools coverage \
  -a /workdir/mbb262/regions_file.bed \
  -b /workdir/mbb262/genotypes/nam/nam_ibm_imputed_intersectMAFFilter_chr1.vcf.gz \
  -counts > /workdir/mbb262/interval_data/snp_counts_all/snp_count_nam_ibm_imputed_chr_1.txt

