#!/bin/bash

# ---------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2021-06-07 
# Updated... 2022-12-27
#
# Description 
#   - Run Linkage Disequlibrium function from tassel for
#	- NAM and 282 population SNPs and then avgerage values across intervals
# ---------------------------------------------------------------

# ------------------------------------------------------------------------------
# Calculate and average LD for NAM - Intervals
# ------------------------------------------------------------------------------

# Get data from cbsu
# /data1/users/mbb262/genotypes/nam/imputed/beagle_intersect_nam/*vcf.gz


# Loop through each chromosome, calculate LD
cd /workdir/mbb262/genotypes/beagle_intersect_nam
for CHROM in {1..10}
do
  echo "Start analyzing chromosome ${CHROM}"
  /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/ld_matrices/ld_nam/debug_calculateLD_chr${CHROM}_nam.log \
    -Xmx195g \
    -maxThreads 39 \
    -importGuess /workdir/mbb262/genotypes/beagle_intersect_nam/nam_ibm_imputed_intersectMAFFilter_chr${CHROM}.vcf.gz -noDepth \
    -ld \
    -ldWinSize 50 \
    -export /workdir/mbb262/ld_matrices/ld_nam/ld_chr${CHROM}_nam.txt
  echo "End analyzing chromosome ${CHROM}"
done


# Average LD across windows --> needs a lm machine
cd /workdir/mbb262/ld_matrices/ld_nam
for CHROM in {1..2}
do
  echo "Start analyzing chromosome ${CHROM}"
  /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -Xmx800g \
    -debug /workdir/mbb262/ld_matrices/mean_ld_nam/debug_avgLD_chr${CHROM}_nam.log \
    -MeanR2FromLDPlugin \
    -intervals /workdir/mbb262/genic_intergenic_intervals_b73_v4.49.csv \
    -ldResultFile /workdir/mbb262/ld_matrices/ld_nam/ld_chr${CHROM}_nam.txt \
    -output /workdir/mbb262/ld_matrices/mean_ld_nam/avg_ld_chr${CHROM}_nam.txt
  echo "End analyzing chromosome ${CHROM}"
done


# ------------------------------------------------------------------------------
# Calculate LD for 282 - Intervals
# ------------------------------------------------------------------------------

# Get data from cbsu
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/genotypes/goodman282/*vcf.gz /workdir/mbb262/goodman282

# Loop through each chromosome, calculate LD --> DONE
for CHROM in {1..10}
do
  echo "Start analyzing chromosome ${CHROM}"
  /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -debug /workdir/mbb262/ld_matrices/debug_calculateLD_chr${CHROM}_282.log \
    -Xmx195g \
    -maxThreads 39 \
    -importGuess /workdir/mbb262/goodman282/hmp321_282_agpv4_merged_chr${CHROM}_imputed_goodman282.vcf.gz -noDepth \
    -ld \
    -ldWinSize 50 \
    -export /workdir/mbb262/ld_matrices/ld_chr${CHROM}_282.txt
  echo "End analyzing chromosome ${CHROM}"
done


# Average LD across windows --> running 7/28 needs an lm machine
for CHROM in {1..10}
do
  echo "Start analyzing chromosome ${CHROM}"
  /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
    -Xmx500g \
    -debug /workdir/mbb262/ld_matrices/mean_ld_282/debug_avgLD_chr${CHROM}_282.log \
    -MeanR2FromLDPlugin \
    -intervals /workdir/mbb262/genic_intergenic_intervals_b73_v4.49.csv \
    -ldResultFile /workdir/mbb262/ld_matrices/ld_282/ld_chr${CHROM}_282.txt \
    -output /workdir/mbb262/ld_matrices/mean_ld_282/avg_ld_chr${CHROM}_282.txt
  echo "End analyzing chromosome ${CHROM}"
done


# NOTE: I TOOK THE LD MATRICES FROM ABOVE AND SUBSETTED OUT THE SITES FOR THE SINGLE SNP LD COUNTS
# BELOW IS JUST AN EXAMPLE

# # ------------------------------------------------------------------------------
# # Calculate and average LD for NAM - SNPs
# # ------------------------------------------------------------------------------

# # Get data from cbsu
# mkdir -p /workdir/mbb262/snps/nam
# mkdir -p /workdir/mbb262/ld_matrices/ld_nam


# # /data1/users/mbb262/genotypes/nam/imputed/beagle_intersect_nam/*vcf.gz
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/genotypes/nam/imputed/beagle_intersect_nam/*vcf.gz /workdir/mbb262/snps/nam

# # Get SNP interval file
# # scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/interval_data_snps/snp_intervals_b73_v4.49.csv /workdir/mbb262

# # Create a simple chromosome position file
# # See: 20b_calc_snp_ld_chr_pos_for_ld_snp.R

# # Loop through each chromosome, subset SNPS, calculate LD
# cd /workdir/mbb262/snps/nam
# for CHROM in {1..10}
# do
#   echo "Start analyzing chromosome ${CHROM}"
#   /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
#     -debug /workdir/mbb262/ld_matrices/ld_nam/debug_calculateLD_chr${CHROM}_nam.log \
#     -Xmx195g \
#     -maxThreads 39 \
#     -importGuess /workdir/mbb262/snps/nam/nam_ibm_imputed_intersectMAFFilter_chr${CHROM}.vcf.gz -noDepth \
#     -FilterSiteBuilderPlugin \
#     -chrPosFile /workdir/mbb262/snps/chrPosFile/ch${CHROM}_chrPos_snp_intervals_b73_v4.49.txt \
#     -endPlugin \
#     -ld \
#     -ldWinSize 50 \
#     -export /workdir/mbb262/ld_matrices/ld_nam/ld_chr${CHROM}_nam.txt
#   echo "End analyzing chromosome ${CHROM}"
# done

# # Combine files, take average by SNP in R: 20b_calc_snp_ld_chr_pos_for_ld_snp.R


# # ------------------------------------------------------------------------------
# # Calculate LD for 282 - Intervals
# # ------------------------------------------------------------------------------

# mkdir -p /workdir/mbb262/ld_matrices/ld_282
# mkdir -p /workdir/mbb262/snps/gap

# # Get data from cbsu
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/genotypes/goodman282/*vcf.gz /workdir/mbb262/snps/gap

# # Loop through each chromosome, filter sites, calculate LD
# for CHROM in {1..10}
# do
#   echo "Start analyzing chromosome ${CHROM}"
#   /home/mbb262/bioinformatics/tassel-5-standalone/run_pipeline.pl \
#     -debug /workdir/mbb262/ld_matrices/ld_282/debug_calculateLD_chr${CHROM}_282.log \
#     -Xmx195g \
#     -maxThreads 39 \
#     -importGuess /workdir/mbb262/snps/gap/hmp321_282_agpv4_merged_chr${CHROM}_imputed_goodman282.vcf.gz -noDepth \
#     -FilterSiteBuilderPlugin \
#     -chrPosFile /workdir/mbb262/snps/chrPosFile/ch${CHROM}_chrPos_snp_intervals_b73_v4.49.txt \
#     -endPlugin \
#     -ld \
#     -ldWinSize 50 \
#     -export /workdir/mbb262/ld_matrices/ld_282/ld_chr${CHROM}_282.txt
#   echo "End analyzing chromosome ${CHROM}"
# done


# # Combine files, take average by SNP in R: 20b_calc_snp_ld_chr_pos_for_ld_snp.R



