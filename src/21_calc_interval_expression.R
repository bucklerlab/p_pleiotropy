# ---------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2021-07-20 
# Updated... 2022-12-27
#
# Description 
#   - Calculaing the average gene expression by interval
#   - and by SNP at the bottom of the script
# ---------------------------------------------------------------


# ----------------------------
# Load in helpful packages
# -----------------------------

library(dplyr)
library(data.table)
library(GenomicRanges)


# ----------------------------------
# Gather genic/intergenic intervals
# ----------------------------------

# Get intervals from blfs1
# scp mbb262@cbsublfs1.tc.cornell.edu:/data1/users/mbb262/haplotype_ranges/genic_intergenic_intervals_b73_v4.49.csv /workdir/mbb262

# Genic and intergenic ranges (not from the phg but from genic and intergenic ranges in v4 b73)
# ranges <- data.table::fread("/workdir/mbb262/genic_intergenic_intervals_b73_v4.49.csv", header = TRUE)
ranges <- data.table::fread("~/../Box/Cornell_PhD/labProjects/hap_gwa/data/interval_data/genic_intergenic_intervals_b73_v4.49.csv", header = TRUE)

# Change column names
colnames(ranges) <- c("seqid", "start", "end", "rr_id")


# ----------------------------------
# Gather gff file
# ----------------------------------

# Get gff file (for gene coordinates) from: 
# wget ftp://ftp.ensemblgenomes.org/pub/plants/release-49/gff3/zea_mays/Zea_mays.B73_RefGen_v4.49.gff3.gz

# Load in file
# gff <- ape::read.gff("/workdir/mbb262/Zea_mays.B73_RefGen_v4.49.gff3.gz", 
#                      na.strings = c(".", "?"), GFF3 = TRUE)
gff <- ape::read.gff("~/../Box/Cornell_PhD/labProjects/hap_gwa/data/interval_data/Zea_mays.B73_RefGen_v4.49.gff3.gz", 
                     na.strings = c(".", "?"), GFF3 = TRUE)

# Remove extra columns
gff <- gff %>% 
  filter(type == "gene", source == "gramene", seqid %in% c(1,2,3,4,5,6,7,8,9,10)) %>% 
  select("seqid", "start", "end", "attributes")

# Parse out gene names
gff$v4_gene <- gsub("ID=gene:", "", gff$attributes)
gff$v4_gene <- gsub(";.*", "", gff$v4_gene)
gff$seqid <- as.numeric(as.character(gff$seqid))


# ------------------------------------
# Parse GENE expression data into intervals
# ------------------------------------

# get data from blfs1
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/expression_dataset_walley/v4_Transcripts_meanLogExpression.csv /workdir/mbb262/data

# Load in expression data from Walley paper
walley <- read.csv("~/../Box/Cornell_PhD/labProjects/hap_gwa/data/interval_data/input_data/v4_Transcripts_meanLogExpression.csv")

# remove extra columns
walley <- walley %>% select(-"X", -"v2_geneIDs", -"duplicated_v4", -"duplicated_v2")

# Merge walley gene IDs with gene coordinate IDs
v4_walley <- merge(x = walley, 
                   y = gff, 
                   by.x = "v4_geneIDs", 
                   by.y = "v4_gene") %>% as.data.table()
dim(v4_walley) # reasonable seeing that many genes did not map from v3 --> v4

# remove duplicate rows based on v4 gene names (I think it just picks the first gene in alphabetical order)
# doing this because multiple v3 genes map to a single v4 gene and I don't want that
v4_walley <- v4_walley[!duplicated(v4_walley$v4_geneIDs), ]
dim(v4_walley)

# Find the Walley v4 genes that are within the genic or intergenic regions
setkey(v4_walley, seqid, start, end)
walley_overlaps <- foverlaps(ranges, v4_walley,
                             by.x = c("seqid","start", "end"), 
                             type = "within",
                             mult = "first")

# Reorganize column names
walley_overlaps <- walley_overlaps %>% relocate("rr_id", "seqid", "i.start","i.end", 
                                                "v4_geneIDs","start","end")

# Rename columns
colnames(walley_overlaps)[1:7] <- c("rr_id", "seqid", "ranges_start","ranges_end", "v4_gene",
                                    "v4_start","v4_end")

# Check that the orginial range file and the new one have the same number of rows
dim(ranges) # expecting 75480 ranges = ranges nrows
dim(walley_overlaps)

# Many ranges have NA expression (i.e. intergenic regions), make those values = 0
walley_overlaps[,8:30][is.na(walley_overlaps[,8:30])] <- 0
walley_overlaps <- walley_overlaps %>% select(-"attributes", -"seqid", -"ranges_start", -"ranges_end")

# Calculate total expression across intervals
walley_overlaps$total_rna_expression_23tissues <- rowSums(walley_overlaps[,-c(1:4)])

# calculate max expression
walley_overlaps$max_rna_expression_23tissues <- apply(walley_overlaps[, 5:27], 1, max)

# calculate mean expression
walley_overlaps$mean_rna_expression_23tissues <- apply(walley_overlaps[, 5:27], 1, mean)


# ------------------------------------
# Parse PROTEIN expression data into intervals
# ------------------------------------


# get data from blfs1
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/expression_dataset_walley/v4_Protein_meanLogExpression.csv /workdir/mbb262/data

# Load in expression data from Walley paper
protein_walley <- read.csv("~/../Box/Cornell_PhD/labProjects/hap_gwa/data/interval_data/input_data/v4_Protein_meanLogExpression.csv")

# remove extra columns
protein_walley <- protein_walley %>% select(-"X", -"v2_geneIDs", -"duplicated_v4", -"duplicated_v2")

# Merge protein_walley gene IDs with gene coordinate IDs
v4_protein_walley <- merge(x = protein_walley, 
                   y = gff, 
                   by.x = "v4_geneIDs", 
                   by.y = "v4_gene") %>% as.data.table()
dim(v4_protein_walley) # reasonable seeing that many genes did not map from v3 --> v4

# remove duplicate rows based on v4 gene names (I think it just picks the first gene in alphabetical order)
# doing this because multiple v3 genes map to a single v4 gene and I don't want that
v4_protein_walley <- v4_protein_walley[!duplicated(v4_protein_walley$v4_geneIDs), ]
dim(v4_protein_walley)

# Find the protein_walley v4 genes that are within the genic or intergenic regions
setkey(v4_protein_walley, seqid, start, end)
protein_walley_overlaps <- foverlaps(ranges, v4_protein_walley,
                             by.x = c("seqid","start", "end"), 
                             type = "within",
                             mult = "first")

# Reorganize column names
protein_walley_overlaps <- protein_walley_overlaps %>% relocate("rr_id", "seqid", "i.start",
                                                                "i.end", "v4_geneIDs","start","end")

# Rename columns
colnames(protein_walley_overlaps)[1:7] <- c("rr_id", "seqid", "ranges_start","ranges_end", 
                                            "v4_gene","v4_start","v4_end")

# Check that the orginial range file and the new one have the same number of rows
dim(ranges) # expecting 75480 ranges = ranges nrows
dim(protein_walley_overlaps)

# Many ranges have NA protein expression (i.e. intergenic regions), make those values = 0
protein_walley_overlaps[,8:30][is.na(protein_walley_overlaps[,8:30])] <- 0

# remove extra columns
protein_walley_overlaps <- protein_walley_overlaps %>% select(-"attributes")

# Calculate total expression across intervals
protein_walley_overlaps$total_protein_expression_23tissues <- rowSums(protein_walley_overlaps[,8:30])

# calculate max expression
protein_walley_overlaps$max_protein_expression_23tissues <- apply(protein_walley_overlaps[, 8:30], 1, max)

# calculate mean expression
protein_walley_overlaps$mean_protein_expression_23tissues <- apply(protein_walley_overlaps[, 8:30], 1, mean)


# ------------------------------------
# Combine RNA and Protein information
# ------------------------------------

# Merge protein with gene expression data
protein_rna <- merge(x = protein_walley_overlaps, 
                     y = walley_overlaps, 
                     by = "rr_id", 
                     suffixes = c("_protein","_rna"))

# Save this to file
data.table::fwrite(protein_rna, file = "~/../Box/Cornell_PhD/labProjects/hap_gwa/data/interval_data/walley_rna_protein_ranges_overlaps.csv")

# Subset to just the total, maz, and mean expression values
protein_rna_temp <- protein_rna |> select("rr_id","seqid", "ranges_start", "ranges_end",
                                          "v4_gene_protein",
                                          "v4_start_protein","v4_end_protein",
                                          "total_protein_expression_23tissues",
                                          "max_protein_expression_23tissues",
                                          "mean_protein_expression_23tissues",
                                          "total_rna_expression_23tissues",
                                          "max_rna_expression_23tissues",
                                          "mean_rna_expression_23tissues")

data.table::fwrite(protein_rna_temp, file = "~/../Box/Cornell_PhD/labProjects/hap_gwa/data/interval_data/walley_rna_protein_ranges_overlaps_meanMaxTotal.csv")



# ------------------------------------------------------------------------------
#                            SNP-level expression
# ------------------------------------------------------------------------------

# Load in helpful packages
library(dplyr)
library(data.table)
library(GenomicRanges)


# ----------------------------------
# Gather SNP intervals intervals
# ----------------------------------

# Get snps from blfs1
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/interval_data_snps/snp_intervals_b73_v4.49.csv /workdir/mbb262

# load file
ranges <- data.table::fread("/workdir/mbb262/snp_intervals_b73_v4.49.csv", header = TRUE)

# Change column names
colnames(ranges) <- c("seqid", "start", "end", "rr_id")


# ----------------------------------
# Gather & format gff file
# ----------------------------------

# Get gff file (for gene coordinates) from: 
# wget ftp://ftp.ensemblgenomes.org/pub/plants/release-49/gff3/zea_mays/Zea_mays.B73_RefGen_v4.49.gff3.gz

# Load in file
gff <- ape::read.gff("/workdir/mbb262/Zea_mays.B73_RefGen_v4.49.gff3.gz",
                     na.strings = c(".", "?"), GFF3 = TRUE)

# Remove extra columns
gff <- gff %>% 
  filter(type == "gene", source == "gramene", seqid %in% c(1,2,3,4,5,6,7,8,9,10)) %>% 
  select("seqid", "start", "end", "attributes")

# Parse out gene names
gff$v4_gene <- gsub("ID=gene:", "", gff$attributes)
gff$v4_gene <- gsub(";.*", "", gff$v4_gene)
gff$seqid <- as.numeric(as.character(gff$seqid))


# ------------------------------------
# Parse RNA expression data by snp
# ------------------------------------

# get data from blfs1
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/interval_data/expression/v4_Transcripts_meanLogExpression.csv /local/workdir/mbb262/snp_interval_data

# Load in expression data from Walley paper
walley <- read.csv("/workdir/mbb262/snp_interval_data/v4_Transcripts_meanLogExpression.csv")

# remove extra columns
walley <- walley %>% select(-"X", -"v2_geneIDs", -"duplicated_v4", -"duplicated_v2")

# calculate max expression
walley$max_rna_expression_23tissues <- apply(walley[, 2:ncol(walley)], 1, max)

# Subset columns
walley <- walley %>% select("v4_geneIDs", "max_rna_expression_23tissues")

# Merge walley gene IDs with gene coordinate IDs
v4_walley <- merge(x = walley, 
                   y = gff, 
                   by.x = "v4_geneIDs", 
                   by.y = "v4_gene") %>% as.data.table()
dim(v4_walley) # reasonable seeing that many genes did not map from v3 --> v4, also many may not be expressed

# remove duplicate rows based on v4 gene names (I think it just picks the first gene in alphabetical order)
# doing this because multiple v3 genes map to a single v4 gene and I don't want that
v4_walley <- v4_walley[!duplicated(v4_walley$v4_geneIDs), ]
dim(v4_walley)

# Find the Walley v4 genes that are within the 5 kb +/- of a SNP
window_size <- 5000
v4_walley$start <- v4_walley$start - window_size
v4_walley$end <- v4_walley$end + window_size
setkey(v4_walley, seqid, start, end)
walley_overlaps <- foverlaps(ranges, v4_walley,
                             by.x = c("seqid","start", "end"), 
                             type = "within",
                             mult = "first")

# Reorganize column names
walley_overlaps <- walley_overlaps %>% relocate("rr_id", "seqid", "i.start","i.end", 
                                                "v4_geneIDs","start","end")

# Rename columns
colnames(walley_overlaps)[1:7] <- c("rr_id", "seqid", "ranges_start","ranges_end", "v4_gene",
                                    "v4_start","v4_end")

# Check that the orginial range file and the new one have the same number of rows
dim(ranges) # expecting 1M ranges = ranges nrows
dim(walley_overlaps)

# Many ranges have NA expression (i.e. intergenic regions), make those values = 0
# walley_overlaps[,8][is.na(walley_overlaps[,8])] <- 0 # not doing this
walley_overlaps <- walley_overlaps %>% select(-"attributes", -"seqid", -"ranges_start", -"ranges_end")


# ------------------------------------
# Parse PROTEIN expression data into intervals
# ------------------------------------

# get data from blfs1
# scp mbb262@cbsublfs1.biohpc.cornell.edu:/data1/users/mbb262/pleiotropy/interval_data/expression/v4_Protein_meanLogExpression.csv /local/workdir/mbb262/snp_interval_data

# Load in expression data from Walley paper
protein_walley <- read.csv("/workdir/mbb262/snp_interval_data/v4_Protein_meanLogExpression.csv")

# Calculate protein max expression
protein_walley$max_protein_expression_23tissues <- apply(protein_walley[, 4:26], 1, max)

# remove extra columns
protein_walley <- protein_walley %>% select("v4_geneIDs", "max_protein_expression_23tissues")

# Merge protein_walley gene IDs with gene coordinate IDs
v4_protein_walley <- merge(x = protein_walley, 
                           y = gff, 
                           by.x = "v4_geneIDs", 
                           by.y = "v4_gene") %>% as.data.table()
dim(v4_protein_walley) # reasonable seeing that many genes did not map from v3 --> v4

# remove duplicate rows based on v4 gene names (I think it just picks the first gene in alphabetical order)
# doing this because multiple v3 genes map to a single v4 gene and I don't want that
v4_protein_walley <- v4_protein_walley[!duplicated(v4_protein_walley$v4_geneIDs), ]
dim(v4_protein_walley)

# Find the protein_walley v4 genes that are within 5kb of the snp
window_size <- 5000
v4_protein_walley$start <- v4_protein_walley$start - window_size
v4_protein_walley$end <- v4_protein_walley$end + window_size
setkey(v4_protein_walley, seqid, start, end)
protein_walley_overlaps <- foverlaps(ranges, v4_protein_walley,
                                     by.x = c("seqid","start", "end"), 
                                     type = "within",
                                     mult = "first")

# Reorganize column names
protein_walley_overlaps <- protein_walley_overlaps %>% relocate("rr_id", "seqid", "i.start",
                                                                "i.end", "v4_geneIDs","start","end")

# Rename columns
colnames(protein_walley_overlaps)[1:7] <- c("rr_id", "seqid", "ranges_start","ranges_end", 
                                            "v4_gene","v4_start","v4_end")

# Check that the orginial range file and the new one have the same number of rows
dim(ranges) # expecting 75480 ranges = ranges nrows
dim(protein_walley_overlaps)

# Many ranges have NA protein expression (i.e. intergenic regions), make those values = 0
# protein_walley_overlaps[,8:30][is.na(protein_walley_overlaps[,8:30])] <- 0 # not doing this

# remove extra columns
protein_walley_overlaps <- protein_walley_overlaps %>% select(-"attributes", -"seqid", -"ranges_start", -"ranges_end")


# ------------------------------------
# Combine RNA and Protein information
# ------------------------------------

# Merge protein with gene expression data
protein_rna <- merge(x = protein_walley_overlaps, 
                     y = walley_overlaps, 
                     by = "rr_id", 
                     suffixes = c("_protein","_rna"))

# Save this to file
data.table::fwrite(protein_rna, file = "/workdir/mbb262/snp_interval_data/snp_5kb_walley_rna_protein_ranges_overlaps.csv")
