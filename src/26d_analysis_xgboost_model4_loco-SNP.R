# ------------------------------------------------------------------------------
# Author.... Merritt Khaipho-Burch and Emily Yi
# Contact... mbb262@cornell.edu
# Date...... 2022-12-28
# Updated... 2022-12-29
#
# Description: Model 4 (XGBooost without GO) for SNPs
# Using pleiotropy scores from number of unique traits
# combined with many biological and noise responses in a XGBoost model
# 
# Running 8 models, 4: observed data, 4: permuted data
# Each of the 4 models comes from the 4 population-trait categories
#
# Model:
# pleiotropy score ~ ATACseq count + max RNA + max protein + avg. GERP
#                   + interval type + average LD
#
# Modified to respond to reviewer comments, leave-one-chromosome-
# -out model (LOCO)
# ------------------------------------------------------------------------------

# Load in packages
library(dplyr)
library(tidyr)
library(ggplot2)
library(xgboost)

# set seed
set.seed(2022)

# read in data aggregatied by aggregate_interval.data.R
data_dir <- "/workdir/mbb262/snp_interval_data/"  
all_interval_data <- data.table::fread(paste0(data_dir, "/all_interval_data_unique_filter_snp.csv"))


# -----------------------------------------------------------------------------
#                           Format data
# Partition data into Observed Data and Permuted sets
# and separate population-trait categories
# ------------------------------------------------------------------------------

# -----------------------------------------
# NAM Field Observed Data
# -----------------------------------------

# Partition data
nf_obs <- all_interval_data %>% select("seqid",
                                       "average_r2_ld_nam_snp",
                                       "gerp_score",
                                       "max_protein_expression_23tissues",
                                       "max_rna_expression_23tissues",
                                       "atac_seq_peak_count",
                                       "snp_type",
                                       "nam_filtered_all_uniqueCount")
dim(nf_obs) # should be: 1M 8
nf_obs$snp_type <- as.factor(nf_obs$snp_type)


# -----------------------------------------
# NAM Field Permuted
# -----------------------------------------

nam_melt <- all_interval_data %>% 
  select("seqid",
         "snp_type", 
         "average_r2_ld_nam_snp",
         "gerp_score",
         "max_protein_expression_23tissues",
         "max_rna_expression_23tissues",
         "atac_seq_peak_count", 
         "npp_unique_perm_1","npp_unique_perm_2","npp_unique_perm_3","npp_unique_perm_4","npp_unique_perm_5",
         "npp_unique_perm_6","npp_unique_perm_7","npp_unique_perm_8","npp_unique_perm_9","npp_unique_perm_10")

# Melt dataset
nf_permuted <- tidyr::pivot_longer(nam_melt,
                                      cols = npp_unique_perm_1:npp_unique_perm_10,
                                      names_to = "pop_trait_perm_no_perm_level",
                                      values_to = "pleiotropy_npp") %>% select(-"pop_trait_perm_no_perm_level")

dim(nf_permuted) # should be: 10M 8
nf_permuted$snp_type <- as.factor(nf_permuted$snp_type)


# -----------------------------------------
# Goodman Field Observed Data
# -----------------------------------------

gf_obs <- all_interval_data %>% select("seqid",
                                       "average_r2_ld_282_snp",
                                       "gerp_score",
                                       "max_protein_expression_23tissues",
                                       "max_rna_expression_23tissues",
                                       "atac_seq_peak_count",
                                       "snp_type",
                                       "goodman_filtered_physiological_uniqueCount")
dim(gf_obs) # should be: 1M 8
gf_obs$snp_type <- as.factor(gf_obs$snp_type)


# -----------------------------------------
# Goodman Field Permuted
# -----------------------------------------

gap_field_melt <- all_interval_data %>% 
  select("seqid",
         "snp_type", 
         "average_r2_ld_282_snp",
         "gerp_score",
         "max_protein_expression_23tissues",
         "max_rna_expression_23tissues",
         "atac_seq_peak_count",
         "gpp_unique_perm_1","gpp_unique_perm_2","gpp_unique_perm_3","gpp_unique_perm_4","gpp_unique_perm_5",
         "gpp_unique_perm_6","gpp_unique_perm_7","gpp_unique_perm_8","gpp_unique_perm_9","gpp_unique_perm_10")

# Melt dataset
gf_permuted <- tidyr::pivot_longer(gap_field_melt,
                                          cols = gpp_unique_perm_1:gpp_unique_perm_10,
                                          names_to = "pop_trait_perm_no_perm_level",
                                          values_to = "pleiotropy_gpp") %>% select(-"pop_trait_perm_no_perm_level")

dim(gf_permuted) # should be: 10M 8
gf_permuted$snp_type <- as.factor(gf_permuted$snp_type)


# -----------------------------------------
# Goodman Mass Features Observed Data
# -----------------------------------------

gm_obs <- all_interval_data %>% select("seqid",
                                       "average_r2_ld_282_snp",
                                       "gerp_score",
                                       "max_protein_expression_23tissues",
                                       "max_rna_expression_23tissues",
                                       "atac_seq_peak_count",
                                       "snp_type",
                                       "goodman_filtered_metabolite_uniqueCount")
dim(gm_obs) # should be: 1M 8
gm_obs$snp_type <- as.factor(gm_obs$snp_type)


# -----------------------------------------
# Goodman Mass Features Permuted
# -----------------------------------------

gap_mass_melt <- all_interval_data %>% 
  select("seqid",
         "snp_type", 
         "average_r2_ld_282_snp",
         "gerp_score",
         "max_protein_expression_23tissues",
         "max_rna_expression_23tissues",
         "atac_seq_peak_count",
         "gpm_perm_1","gpm_perm_2","gpm_perm_3","gpm_perm_4","gpm_perm_5",
         "gpm_perm_6","gpm_perm_7","gpm_perm_8","gpm_perm_9","gpm_perm_10")

# Melt dataset
gm_permuted <- tidyr::pivot_longer(gap_mass_melt,
                                         cols = gpm_perm_1:gpm_perm_10,
                                         names_to = "pop_trait_perm_no_perm_level",
                                         values_to = "pleiotropy_gpm") %>% select(-"pop_trait_perm_no_perm_level")

dim(gm_permuted) # should be: 10M 8
gm_permuted$snp_type <- as.factor(gm_permuted$snp_type)


# -----------------------------------------
# Goodman expression Observed Data
# -----------------------------------------

ge_obs <- all_interval_data %>% select("seqid",
                                       "average_r2_ld_282_snp",
                                       "gerp_score",
                                       "max_protein_expression_23tissues",
                                       "max_rna_expression_23tissues",
                                       "atac_seq_peak_count",
                                       "snp_type",
                                       "goodman_filtered_expression_uniqueCount")
dim(ge_obs) # should be: 
ge_obs$snp_type <- as.factor(ge_obs$snp_type)


# -----------------------------------------
# Goodman Expression Permuted
# -----------------------------------------

gap_exp_melt <- all_interval_data %>% 
  select("seqid",
         "snp_type", 
         "average_r2_ld_282_snp",
         "gerp_score",
         "max_protein_expression_23tissues",
         "max_rna_expression_23tissues",
         "atac_seq_peak_count",
         "gpe_perm_1","gpe_perm_2","gpe_perm_3","gpe_perm_4","gpe_perm_5")

# Melt data
ge_permuted <- tidyr::pivot_longer(gap_exp_melt,
                                               cols = gpe_perm_1:gpe_perm_5,
                                               names_to = "pop_trait_perm_no_perm_level",
                                               values_to = "pleiotropy_gpe") %>% select(-"pop_trait_perm_no_perm_level")

dim(ge_permuted) # should be: 5M 8
ge_permuted$snp_type <- as.factor(ge_permuted$snp_type)


# ------------------------------------------------------------------------------
# Split training and testing data
# Run a leave-one-chromosome-out model
# Iterate through what chromosome is left out
# Train chrom c(2-10),     test 1
# Train chrom c(1,3-10),   test 2
# Train chrom c(1-2,4-10), test 3
# Train chrom c(1-3,5-10), test 4
# Train chrom c(1-4,6-10), test 5
# Train chrom c(1-5,7-10), test 6
# Train chrom c(1-6,8-10), test 7
# Train chrom c(1-7,9-10), test 8
# Train chrom c(1-8,10),   test 9
# Train chrom c(1-9),      test 10
#
# Doing this through the case weights and holdout method in ranger::ranger()
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Format names to look nicer for RF model outputs
# ------------------------------------------------------------------------------

# load in table with formatted names
pretty_names <- data.table::fread("/home/mbb262/git_projects/pleiotropy/data/pretty_names_for_rf.csv")


# ------------------------------------------------------------------------------
# Create a function that takes in data, runs LOCO, exports outputs
# ------------------------------------------------------------------------------
# Create a list with the information of what chroms to test, and what to leave out
training <- list(c(2:10),
                 c(1,3:10),
                 c(1:2,4:10),
                 c(1:3,5:10),
                 c(1:4,6:10),
                 c(1:5,7:10),
                 c(1:6,8:10),
                 c(1:7,9:10),
                 c(1:8,10),
                 c(1:9))
testing <- seq(1:10)

# Create a for loop within a function that iterate through what chromosome is left out and:
# Step 1: Separates the training and test data
# Step 2: Runs the random forest models
# Step 3: Prep and saves information from the random forest models for the feature importance plot
# Step 4: Create predictions on left out chromosome
# Step 5: Save predictions and importance values outside of loop
# Step 6: Calculate standard error of relative importance results

loco_xg_model4 <- function(df, pop) {
  
  # Collect results
  df_output_relative_importance <- c()
  df_output_predicted_values <- c()
  df_output_model_performance <- c()
  
  for (i in 1:length(testing)) {
    # Print a status message
    print(paste0("Training on chroms: ", training[i], "; Testing on chrom: ", testing[i]))
    
    # Step 1: Separate the training and test data
    train_obs <- df %>% filter(seqid != testing[i])
    test_obs <- df %>% filter(seqid == testing[i])
    
    # data is always in the 11th column
    print(paste0("Trait: ", colnames(df[,8])))
    train_data_mat <- train_obs[,-c(1,8)] %>% data.matrix()
    train_label_mat <- train_obs[,8] %>% data.matrix()
    test_data_mat <- test_obs[,-c(1,8)] %>% data.matrix()
    test_label_mat <- test_obs[,8] %>% data.matrix()

    # Step 2: Run the xgboost models
    dtrain <- xgboost::xgb.DMatrix(data = train_data_mat, label = train_label_mat)
    dtest <- xgboost::xgb.DMatrix(data = test_data_mat, label = test_label_mat)
    watchlist <- list(train=dtrain, test=dtest)
    boost <- xgb.train(data = dtrain, max.depth=6, eta=0.5, nthread = 2, nrounds=8, watchlist = watchlist)
    
    # Step 3: Prep and saves information from the random forest models for the feature importance plot
    xg_feature_importance <- xgb.importance(model = boost) %>% as.data.frame()
    colnames(xg_feature_importance)[1:2] <- c("Variable", "Importance")
    xg_feature_importance$Importance <- xg_feature_importance$Importance/max(xg_feature_importance$Importance)
    xg_feature_importance <- merge(x = xg_feature_importance, y = pretty_names, by.x = "Variable", by.y = "old", all.x = TRUE) %>%
      select(-c("Cover", "Frequency"))
    colnames(xg_feature_importance) <- c("Variable_old", "Importance", "Variable", "Type")
    
    # Step 4: Create predictions on left out chromosome
    test_obs$predictedChromValues <- predict(boost, dtest)
    test_obs$heldOutChrom <- rep(testing[i], nrow(test_obs)) # add info on what chromosome this is holding out
    
    # Get model performance stats (MSE and R2), chrom left out
    temp <- data.frame(mean((test_obs$predictedChromValues - test_label_mat)^2), 
                       cor(test_obs$predictedChromValues, test_label_mat)^2,
                       testing[i]) 
    colnames(temp) <- c("prediction.error", "r.squared", "chromLeftOut")
    
    # Step 5: Save predictions, relative importance values, and model performance outside of loop
    df_output_relative_importance <- rbind(df_output_relative_importance, xg_feature_importance)
    df_output_predicted_values <- rbind(df_output_predicted_values, test_obs)
    df_output_model_performance <- rbind(df_output_model_performance, temp)
    
  }
  # Step 6: Calculate standard error of relative importance results
  df_output_relative_importance <- df_output_relative_importance %>% 
    group_by(Variable_old) %>%
    summarise(mean = mean(Importance), se = sd(Importance)/sqrt(n()))
  
  # Add back in pretty names for RF results
  df_output_relative_importance <- merge(x = df_output_relative_importance, y = pretty_names,
                                         by.x = "Variable_old", by.y = "old", all.x = TRUE)
  print(df_output_relative_importance)
  # Return results
  return(list(df_output_relative_importance = df_output_relative_importance, 
              df_output_predicted_values = df_output_predicted_values,
              xg = df_output_model_performance))
}


# ------------------------------------------------------------------------------
# Use the function on different pops and data
# ------------------------------------------------------------------------------

# NAM Field - Observed Data
nf_observed_model4 <- loco_xg_model4(df = nf_obs)

# NAM Field - Permuted
nf_perm_model4 <- loco_xg_model4(df = nf_permuted)

# 282 Field - Observed Data
gf_observed_model4 <- loco_xg_model4(df = gf_obs)

# 282 Field - Permuted
gf_perm_model4 <- loco_xg_model4(df = gf_permuted)

# 282 Mass Features - Observed Data
gm_observed_model4 <- loco_xg_model4(df = gm_obs)

# 282 Mass Features - Permuted
gm_perm_model4 <- loco_xg_model4(df = gm_permuted)

# 282 expression - Observed Data
ge_observed_model4 <- loco_xg_model4(df = ge_obs)

# 282 expression - Permuted
ge_perm_model4 <- loco_xg_model4(df = ge_permuted)


# ------------------------------------------------------------------------------
# Make a table summarizing results (aggregate data from across models)
# ------------------------------------------------------------------------------

# Assemble model results
temp <- rbind(nf_observed_model4[[3]], 
              nf_perm_model4[[3]],
              gf_observed_model4[[3]],
              gf_perm_model4[[3]],
              gm_observed_model4[[3]],
              gm_perm_model4[[3]],
              ge_observed_model4[[3]],
              ge_perm_model4[[3]])
temp$model_id <- c(rep("NAM Field Observed Data", 10), 
                   rep("NAM Field Permuted Data", 10),
                   rep("GAP Field Observed Data", 10),
                   rep("GAP Field Permuted Data", 10),
                   rep("GAP Mass Features Observed Data", 10),
                   rep("GAP Mass Features Permuted Data", 10),
                   rep("GAP Expression Observed Data", 10),
                   rep("GAP Expression Permuted Data", 10))

# rearrange and make nicer names
temp <- temp[,c(4,1,2,3)]
colnames(temp) <- c("model", "prediction_error", "r_squared", "left_out_chrom")

# Export
data.table::fwrite(temp, "~/git_projects/pleiotropy/data/model4_XG_prediction_accuracy_loco_snps.csv")

# Look at mean prediction accuracy
temp %>% group_by(model) %>% 
  summarise(mean_prediction_error = mean(prediction_error),
            se_prediction_error = sd(prediction_error)/sqrt(n()),
            mean_r_squared = mean(r_squared),
            se_r_squared = sd(r_squared)/sqrt(n()))


# Save model results to an R Object
save(nf_observed_model4, nf_perm_model4, 
     gf_observed_model4, gf_perm_model4,
     gm_observed_model4, gm_perm_model4,
     ge_observed_model4, ge_perm_model4,
     file = "/home/mbb262/git_projects/pleiotropy/data/model4_loco_data_snp.RData")

# Load data if saved previously
load("~/git_projects/pleiotropy/data/model4_loco_data_snp.RData")


# -------------------------
# Set plot size parameters
# -------------------------

# PLOS genetics
axis_text_size <- 7
title_text_size <- 8
tag_size <- 11
legend_text_size <- 7
legend_shape_size <- 0.65
r2_text_x <- 0
annotate_size <- 3.2


# The color blind palette with grey:
cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")


# --------------------------------------------------------------------------------
#                         Feature importance plots 
# --------------------------------------------------------------------------------

# NAM field Observed Data -----------------------------------------------
a_model4 <- nf_observed_model4$df_output_relative_importance %>% 
  dplyr::arrange(desc(mean)) %>%
  ggplot(aes(x= reorder(Variable, mean), y = mean, fill = Type)) +
  geom_col() +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9)) +
  coord_flip() +
  ggtitle("NAM Field Observed SNP") +
  xlab("Variable Type") +
  ylab("Mean Relative Importance") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=c("#999999", "#56B4E9","#E69F00")) +
  scale_fill_manual(values=c("#999999", "#56B4E9", "#E69F00")) +
  labs(tag = "A")


# NAM field Permuted -------------------------------------------------
b_model4 <- nf_perm_model4$df_output_relative_importance %>% 
  dplyr::arrange(desc(mean)) %>%
  ggplot(aes(x= reorder(Variable, mean), y = mean, fill = Type)) +
  geom_col() +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9)) +
  coord_flip() +
  ggtitle("NAM Field Permuted SNP") +
  xlab("Variable Type") +
  ylab("Mean Relative Importance") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=c("#999999", "#56B4E9","#E69F00")) +
  scale_fill_manual(values=c("#999999", "#56B4E9", "#E69F00")) +
  labs(tag = "B")

# Goodman Field Observed Data ----------------------------------
c_model4 <- gf_observed_model4$df_output_relative_importance %>% 
  dplyr::arrange(desc(mean)) %>%
  ggplot(aes(x= reorder(Variable, mean), y = mean, fill = Type)) +
  geom_col() +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9)) +
  coord_flip() +
  ggtitle("GAP Field Observed SNP") +
  xlab("Variable Type") +
  ylab("Mean Relative Importance") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=c("#999999", "#56B4E9","#E69F00")) +
  scale_fill_manual(values=c("#999999", "#56B4E9", "#E69F00")) +
  labs(tag = "C")

# Goodman Field Permuted ------------------------------------
d_model4 <- gf_perm_model4$df_output_relative_importance %>% 
  dplyr::arrange(desc(mean)) %>%
  ggplot(aes(x= reorder(Variable, mean), y = mean, fill = Type)) +
  geom_col() +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9)) +
  coord_flip() +
  ggtitle("GAP Field Permuted SNP") +
  xlab("Variable Type") +
  ylab("Mean Relative Importance") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=c("#999999", "#56B4E9","#E69F00")) +
  scale_fill_manual(values=c("#999999", "#56B4E9", "#E69F00")) +
  labs(tag = "D")

# Goodman Mass Features Observed Data -------------------------------------
e_model4 <- gm_observed_model4$df_output_relative_importance %>% 
  dplyr::arrange(desc(mean)) %>%
  ggplot(aes(x= reorder(Variable, mean), y = mean, fill = Type)) +
  geom_col() +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9)) +
  coord_flip() +
  ggtitle("GAP Mass Features Observed SNP") +
  xlab("Variable Type") +
  ylab("Mean Relative Importance") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=c("#999999", "#56B4E9","#E69F00")) +
  scale_fill_manual(values=c("#999999", "#56B4E9", "#E69F00")) +
  labs(tag = "E")

# Goodman Mass Features Permuted --------------------------------------
f_model4 <- gm_perm_model4$df_output_relative_importance %>% 
  dplyr::arrange(desc(mean)) %>%
  ggplot(aes(x= reorder(Variable, mean), y = mean, fill = Type)) +
  geom_col() +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9)) +
  coord_flip() +
  ggtitle("GAP Mass Features Permuted SNP") +
  xlab("Variable Type") +
  ylab("Mean Relative Importance") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=c("#999999", "#56B4E9","#E69F00")) +
  scale_fill_manual(values=c("#999999", "#56B4E9", "#E69F00")) +
  labs(tag = "F")

# goodman expression Observed Data ------------------------------------
g_model4 <- ge_observed_model4$df_output_relative_importance %>% 
  dplyr::arrange(desc(mean)) %>%
  ggplot(aes(x= reorder(Variable, mean), y = mean, fill = Type)) +
  geom_col() +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9)) +
  coord_flip() +
  ggtitle("GAP Expression Observed SNP") +
  xlab("Variable Type") +
  ylab("Mean Relative Importance") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=c("#999999", "#56B4E9")) +
  scale_fill_manual(values=c("#999999", "#56B4E9")) +
  labs(tag = "G")

# goodman expression Permuted --------------------------------------
h_model4 <- ge_perm_model4$df_output_relative_importance %>% 
  dplyr::arrange(desc(mean)) %>%
  ggplot(aes(x= reorder(Variable, mean), y = mean, fill = Type)) +
  geom_col() +
  geom_errorbar(aes(ymin=mean-se, ymax=mean+se), width=.2, position=position_dodge(.9)) +
  coord_flip() +
  ggtitle("GAP Expression Permuted SNP") +
  xlab("Variable Type") +
  ylab("Mean Relative Importance") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=c("#999999", "#56B4E9")) +
  scale_fill_manual(values=c("#999999", "#56B4E9")) +
  labs(tag = "H")


# Export to file----------------------------------------------------------------
ggsave("~/git_projects/pleiotropy/images/model4_random_forest_importance_observed_vs_permuted_loco_snp.tiff",
       plot = ggpubr::ggarrange(a_model4,b_model4,c_model4,d_model4,
                                e_model4,f_model4,g_model4,h_model4,
                                nrow = 4, ncol = 2, common.legend = TRUE, legend = "bottom", align = "v"),
       width = 7.5,
       height = 8.75,
       units = "in",
       dpi = "retina")

# # Observed data
# ggsave("~/git_projects/pleiotropy/images/model4_random_forest_importance_observed_loco_snp.png",
#        plot = ggpubr::ggarrange(a_model4,c_model4,e_model4,g_model4,
#                                 nrow = 2, ncol = 2,
#                                 common.legend = TRUE, legend = "bottom", align = "v"),
#        width = 18,
#        height = 14,
#        units = "in",
#        dpi = "retina")
# 
# # Permuted data
# ggsave("~/git_projects/pleiotropy/images/model4_random_forest_importance_permuted_loco_snp.png",
#        plot = ggpubr::ggarrange(b_model4,d_model4,f_model4,h_model4,
#                                 nrow = 2, ncol = 2,
#                                 common.legend = TRUE, legend = "bottom", align = "v"),
#        width = 18,
#        height = 14,
#        units = "in",
#        dpi = "retina")


# --------------------------------------------------------------------------------
# Plot & combine all model performance plots 
# --------------------------------------------------------------------------------

# NAM Field Observed Data --------------------------------------------------------------
test2 <- paste("~R^2==~", round(cor(nf_observed_model4$df_output_predicted_values$nam_filtered_all_uniqueCount, 
                                    nf_observed_model4$df_output_predicted_values$predictedChromValues)^2, digits = 3))

a_pred_model4 <- ggplot(nf_observed_model4$df_output_predicted_values, aes(y = nam_filtered_all_uniqueCount, x = predictedChromValues)) +
  geom_point(colour = "#999999") +
  geom_smooth(method = lm, color = "black", linetype = "solid") + 
  geom_abline(slope = 1, intercept = 0, color = "black", linetype = "dashed") +
  labs(y = "Observed Pleiotropy", 
       x = "Predicted Pleiotropy", 
       title = "NAM Field Observed SNP") +
  annotate(geom="text", label = test2, x = r2_text_x, y = 35, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  ylim(-1,40) +
  xlim(0,40) +
  labs(tag = "A")


# NAM Field Permuted-----------------------------------------------------------------
test2 <- paste("~R^2==~", 
               round(cor(nf_perm_model4$df_output_predicted_values$pleiotropy_npp, 
                         nf_perm_model4$df_output_predicted_values$predictedChromValues)^2, digits = 3))

# Randomly subsample results so this doesnt take forever to plot
nf_perm_m4_df <- nf_perm_model4$df_output_predicted_values
nf_perm_m4_df <- nf_perm_m4_df[sample(nrow(nf_perm_m4_df), 500000), ]

b_pred_model4 <- ggplot(nf_perm_m4_df, aes(y = pleiotropy_npp, x = predictedChromValues)) +
  geom_point(colour = "#999999") +
  geom_smooth(method = lm, color = "black") + 
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") +
  labs(y = "Permuted Pleiotropy", 
       x = "Predicted Pleiotropy", 
       title = "NAM Field Permuted SNP") +
  annotate(geom="text", label = test2, x = r2_text_x, y = 35, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  ylim(-1,40) +
  xlim(0,40) +
  labs(tag = "B")

# Goodman Field Observed Data--------------------------------------------------
test2 <- paste("~R^2==~", 
               round(cor(gf_observed_model4$df_output_predicted_values$goodman_filtered_physiological_uniqueCount, 
                         gf_observed_model4$df_output_predicted_values$predictedChromValues)^2, digits = 3))

c_pred_model4 <- ggplot(gf_observed_model4$df_output_predicted_values, aes(y = goodman_filtered_physiological_uniqueCount, x = predictedChromValues)) +
  geom_point(colour = "#999999") +
  geom_smooth(method = lm, color = "black") + 
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") +
  labs(y = "Observed Pleiotropy", 
       x = "Predicted Pleiotropy", 
       title = "GAP Field Observed SNP") +
  annotate(geom="text", label = test2, x = r2_text_x, y = 35, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  ylim(-1,40) +
  xlim(0,40) +
  labs(tag = "C")

# Goodman Field Permuted---------------------------------------------------
test2 <- paste("~R^2==~", round(cor(gf_perm_model4$df_output_predicted_values$pleiotropy_gpp, 
                                    gf_perm_model4$df_output_predicted_values$predictedChromValues)^2, digits = 3))

# Randomly subsample results so this doesnt take forever to plot
df_perm_m4_df <- gf_perm_model4$df_output_predicted_values
df_perm_m4_df <- df_perm_m4_df[sample(nrow(df_perm_m4_df), 500000), ]

d_pred_model4 <- ggplot(df_perm_m4_df, aes(y = pleiotropy_gpp, x = predictedChromValues)) +
  geom_point(colour = "#999999") +
  geom_smooth(method = lm, color = "black") + 
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") +
  labs(y = "Permuted Pleiotropy", 
       x = "Predicted Pleiotropy", 
       title = "GAP Field Permuted SNP") +
  annotate(geom="text", label = test2, x = r2_text_x, y = 35, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  ylim(-1,40) +
  xlim(0,40) +
  labs(tag = "D")


# Goodman Mass Features Observed Data----------------------------------------------------
test2 <- paste("~R^2==~", round(cor(gm_observed_model4$df_output_predicted_values$goodman_filtered_metabolite_uniqueCount, 
                                    gm_observed_model4$df_output_predicted_values$predictedChromValues)^2, digits = 3))

e_pred_model4 <- ggplot(gm_observed_model4$df_output_predicted_values, aes(y = goodman_filtered_metabolite_uniqueCount, x = predictedChromValues)) +
  geom_point(colour = "#999999") +
  geom_smooth(method = lm, color = "black") + 
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") +
  labs(y = "Observed Pleiotropy", 
       x = "Predicted Pleiotropy", 
       title = "GAP Mass Features Observed SNP") +
  annotate(geom="text", label = test2, x = r2_text_x, y = 230, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  ylim(-1,250) +
  xlim(0,250) +
  labs(tag = "E")

# Goodman Mass Features Permuted-----------------------------------------------------
test2 <- paste("~R^2==~", round(cor(gm_perm_model4$df_output_predicted_values$pleiotropy_gpm, 
                                    gm_perm_model4$df_output_predicted_values$predictedChromValues)^2, digits = 3))

# Randomly subsample results so this doesnt take forever to plot
temp <- gm_perm_model4$df_output_predicted_values
temp2 <- temp[sample(nrow(temp), 500000), ]

f_pred_model4 <- ggplot(temp2, aes(y = pleiotropy_gpm, x = predictedChromValues)) +
  geom_point(colour = "#999999") +
  geom_smooth(method = lm, color = "black") + 
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") +
  labs(y = "Permuted Pleiotropy", 
       x = "Predicted Pleiotropy", 
       title = "GAP Mass Features Permuted SNP") +
  annotate(geom="text", label = test2, x = r2_text_x, y = 230, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  ylim(-1,250) +
  xlim(0,250) +
  labs(tag = "F")


# Goodman expression Observed Data---------------------------------------------------
test2 <- paste("~R^2==~", round(cor(ge_observed_model4$df_output_predicted_values$goodman_filtered_expression_uniqueCount, 
                                    ge_observed_model4$df_output_predicted_values$predictedChromValues)^2, digits = 3))

g_pred_model4 <- ggplot(ge_observed_model4$df_output_predicted_values, aes(y = goodman_filtered_expression_uniqueCount, x = predictedChromValues)) +
  geom_point(colour = "#999999") +
  geom_smooth(method = lm, color = "black") + 
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") +
  labs(y = "Observed Pleiotropy", 
       x = "Predicted Pleiotropy", 
       title = "GAP Expression Observed SNP") +
  annotate(geom="text", label = test2, x = r2_text_x, y = 950, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  ylim(-1,1000) +
  xlim(0,1000) +
  labs(tag = "G")

# goodman expression Permuted------------------------------------------------------
test2 <- paste("~R^2==~", round(cor(ge_perm_model4$df_output_predicted_values$pleiotropy_gpe, 
                                    ge_perm_model4$df_output_predicted_values$predictedChromValues)^2, digits = 3))

# Randomly subsample results so this doesnt take forever to plot
temp3 <- ge_perm_model4$df_output_predicted_values
temp4 <- temp3[sample(nrow(temp3), 500000), ]

h_pred_model4 <- ggplot(temp4, aes(y = pleiotropy_gpe, x = predictedChromValues)) +
  geom_point(colour = "#999999") +
  geom_smooth(method = lm, color = "black") + 
  geom_abline(slope = 1, intercept = 0, linetype = "dashed") +
  labs(y = "Permuted Pleiotropy", 
       x = "Predicted Pleiotropy", 
       title = "GAP Expression Permuted SNP") +
  annotate(geom="text", label = test2, x = r2_text_x, y = 950, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  ylim(-1,1000) +
  xlim(0,1000) +
  labs(tag = "H")


# Export to file-----------------------------------------------------------------
ggsave("~/git_projects/pleiotropy/images/model4_random_forest_prediction_accuracy_observed_vs_permuted_loco_snp.tiff",
       plot = ggpubr::ggarrange(a_pred_model4,b_pred_model4,c_pred_model4,d_pred_model4, 
                                e_pred_model4,f_pred_model4,g_pred_model4,h_pred_model4,
                                nrow = 4, ncol = 2, 
                                common.legend = TRUE, legend = "bottom", align = "v"),
       width = 7.5,
       height = 8.75,
       units = "in",
       dpi = "retina")
