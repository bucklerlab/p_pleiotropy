# ---------------------------------------------------------------
# Author.... Merritt Khaipho-Burch
# Contact... mbb262@cornell.edu
# Date...... 2021-08-18
# Updated... 2022-04-13
#
# Description 
#   - calculates residual entropy and fits it to max expression
# ---------------------------------------------------------------

# Followed this tutorial to pass variables into functions:
# https://aosmith.rbind.io/2019/06/24/function-for-model-fitting/


# ----------------------------
# Load in helpful packages
# -----------------------------

library(dplyr)
library(data.table)
library(ggplot2)
library(ggpubr)

# Make shortcut for path
data_dir <- "/Volumes/merrittData1/pleiotropy/interval_data"

# Load in melted and unmelted data
all_interval_data_melted <- data.table::fread(paste0(data_dir, "/all_interval_data_melted_unique_filter.csv"))

# subset to just columns needed for plotting
all_interval_data_melted <- all_interval_data_melted %>% 
  select("rr_id", "range_type", "Population", "perm_type", "pop_trait", "is_adjusted", "pleiotropy_score", 
         "max_rna_expression_23tissues", "max_protein_expression_23tissues")

# Filter to only genic ranges
all_interval_data_melted <- all_interval_data_melted %>% 
  filter(range_type == "genic" & is_adjusted == "Residual")


# -------------------------
# Set plot size parameters
# -------------------------

axis_text_size <- 8
title_text_size <- 9.5
tag_size <- 10
filt_text_phys <- 22
ran_text_phys <- 21
filt_text <- 10
ran_text <- 8
r2_text_x <- 0
annotate_size <- 2.5
pointSize <- 0.5
legend_text_size <- 8
legend_shape_size <- 0.6

# The color blind palette with grey:
cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")


# -----------------------------------------------------------
#     Plot RNA Expression for each population-trait category
# -----------------------------------------------------------

# nam physiological --------------------------------------------------------------------
filtered_df <- all_interval_data_melted %>% filter(Population == "NAM")
dim(filtered_df) #[1] 415140      8

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(max_rna_expression_23tissues ~ pleiotropy_score, temp1))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(max_rna_expression_23tissues ~ pleiotropy_score, temp2))

cor_filt <- filtered_df %>% 
  filter(perm_type == "Observed Data") %>% 
  select(pleiotropy_score, max_rna_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test1 <- paste("'Observed'","~R^2==~",round(cor_filt[2]^2, digits = 3))

cor_unfilt <- filtered_df %>% 
  filter(perm_type == "Permuted") %>% 
  select(pleiotropy_score, max_rna_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test2 <- paste("'Permuted'","~R^2==~",round(cor_unfilt[2]^2, digits = 3))

exp1 <- ggplot(filtered_df, aes(x = max_rna_expression_23tissues, y = pleiotropy_score, color = perm_type)) +
  geom_smooth(method = lm) + 
  labs(x = "Max RNA expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "NAM Field") +
  annotate(geom="text", label = test1, x = r2_text_x, y = 0.8, size = annotate_size, parse = TRUE, hjust = 0) +
  annotate(geom="text", label = test2, x = r2_text_x, y = 0.73, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=cbPalette) +
  scale_fill_manual(values=cbPalette) +
  labs(tag = "A") 


# goodman physiological ----------------------------------------------------------------

filtered_df <- all_interval_data_melted %>% filter(pop_trait == "Goodman Physiological")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(max_rna_expression_23tissues ~ pleiotropy_score, temp1))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(max_rna_expression_23tissues ~ pleiotropy_score, temp2))

cor_filt <- filtered_df %>% 
  filter(perm_type == "Observed Data") %>% 
  select(pleiotropy_score, max_rna_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test1 <- paste("'Observed'","~R^2==~",round(cor_filt[2]^2, digits = 3))

cor_unfilt <- filtered_df %>% 
  filter(perm_type == "Permuted") %>% 
  select(pleiotropy_score, max_rna_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test2 <- paste("'Permuted'","~R^2==~",round(cor_unfilt[2]^2, digits = 3))

exp2 <- ggplot(filtered_df, aes(x = max_rna_expression_23tissues, y = pleiotropy_score, color = perm_type)) +
  geom_smooth(method = lm) + 
  labs(x = "Max RNA expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Field") +
  annotate(geom="text", label = test1, x = r2_text_x, y = .4, size = annotate_size, parse = TRUE, hjust = 0) +
  annotate(geom="text", label = test2, x = r2_text_x, y = .37, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=cbPalette) +
  scale_fill_manual(values=cbPalette) +
  labs(tag = "B") 


# goodman metabolite ------------------------------------------------------------------

filtered_df <- all_interval_data_melted %>% filter(pop_trait == "Goodman Metabolite")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(max_rna_expression_23tissues ~ pleiotropy_score, temp1))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(max_rna_expression_23tissues ~ pleiotropy_score, temp2))

cor_filt <- filtered_df %>% 
  filter(perm_type == "Observed Data") %>% 
  select(pleiotropy_score, max_rna_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test1 <- paste("'Observed'","~R^2==~",round(cor_filt[2]^2, digits = 3))

cor_unfilt <- filtered_df %>% 
  filter(perm_type == "Permuted") %>% 
  select(pleiotropy_score, max_rna_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test2 <- paste("'Permuted'","~R^2==~",round(cor_unfilt[2]^2, digits = 3))

exp3 <- ggplot(filtered_df, aes(x = max_rna_expression_23tissues, y = pleiotropy_score, color = perm_type)) +
  geom_smooth(method = lm) + 
  labs(x = "Max RNA expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Mass Features") +
  annotate(geom="text", label = test1, x = r2_text_x, y = 10, size = annotate_size, parse = TRUE, hjust = 0) +
  annotate(geom="text", label = test2, x = r2_text_x, y = 9.2, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=cbPalette) +
  scale_fill_manual(values=cbPalette) +
  labs(tag = "C") 


# goodman expression ------------------------------------------------------------------

filtered_df <- all_interval_data_melted %>% filter(pop_trait == "Goodman Expression")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(max_rna_expression_23tissues ~ pleiotropy_score, temp1))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(max_rna_expression_23tissues ~ pleiotropy_score, temp2))

cor_filt <- filtered_df %>% 
  filter(perm_type == "Observed Data") %>% 
  select(pleiotropy_score, max_rna_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test1 <- paste("'Observed'","~R^2==~",round(cor_filt[2]^2, digits = 3))

cor_unfilt <- filtered_df %>% 
  filter(perm_type == "Permuted") %>% 
  select(pleiotropy_score, max_rna_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test2 <- paste("'Permuted'","~R^2==~",round(cor_unfilt[2]^2, digits = 3))

exp4 <- ggplot(filtered_df, aes(x = max_rna_expression_23tissues, y = pleiotropy_score, color = perm_type)) +
  geom_smooth(method = lm) + 
  labs(x = "Max RNA expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Expression") +
  annotate(geom="text", label = test1, x = r2_text_x, y = 12, size = annotate_size, parse = TRUE, hjust = 0) +
  annotate(geom="text", label = test2, x = r2_text_x, y = 11, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=cbPalette) +
  scale_fill_manual(values=cbPalette) +
  labs(tag = "D")  


# Look at all plots together ------------------------------------------------------------
# ggpubr::ggarrange(exp1,exp2,exp3,exp4, nrow = 2, ncol = 2, 
#                   common.legend = TRUE, legend = "bottom")

# Export to file ------------------------------------------------------------------------
ggsave("~/git_projects/pleiotropy/images/adjusted_pleiotropy_vs_rna_expression.png",
       plot = ggpubr::ggarrange(exp1,exp2,exp3,exp4, nrow = 2, ncol = 2, 
                                common.legend = TRUE, legend = "bottom", align = "v"),
       width = 6.5,
       height = 5.5, 
       units = "in")


# --------------------------------------------------------------------------------------------
#     Plot protein Expression for each population-trait category
# --------------------------------------------------------------------------------------------

# nam physiological --------------------------------------------------------------------
filtered_df <- all_interval_data_melted %>% filter(Population == "NAM" & is_adjusted == "Residual")
dim(filtered_df)

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(max_protein_expression_23tissues ~ pleiotropy_score, temp1))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(max_protein_expression_23tissues ~ pleiotropy_score, temp2))
cor_filt <- filtered_df %>% 
  filter(perm_type == "Observed Data") %>% 
  select(pleiotropy_score, max_protein_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test1 <- paste("'Observed'","~R^2==~",round(cor_filt[2]^2, digits = 3))

cor_unfilt <- filtered_df %>% 
  filter(perm_type == "Permuted") %>% 
  select(pleiotropy_score, max_protein_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test2 <- paste("'Permuted'","~R^2==~",round(cor_unfilt[2]^2, digits = 3))

exp1_protein <- ggplot(filtered_df, aes(x = max_protein_expression_23tissues, y = pleiotropy_score, color = perm_type)) +
  geom_smooth(method = lm) + 
  labs(x = "Max protein expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "NAM Field") +
  annotate(geom="text", label = test1, x = r2_text_x, y = 0.9, size = annotate_size, parse = TRUE, hjust = 0) +
  annotate(geom="text", label = test2, x = r2_text_x, y = 0.83, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=cbPalette) +
  scale_fill_manual(values=cbPalette) +
  labs(tag = "A") 


# goodman physiological ----------------------------------------------------------------

filtered_df <- all_interval_data_melted %>% filter(pop_trait == "Goodman Physiological")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(max_protein_expression_23tissues ~ pleiotropy_score, temp1))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(max_protein_expression_23tissues ~ pleiotropy_score, temp2))
cor_filt <- filtered_df %>% 
  filter(perm_type == "Observed Data") %>% 
  select(pleiotropy_score, max_protein_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test1 <- paste("'Observed'","~R^2==~",round(cor_filt[2]^2, digits = 3))

cor_unfilt <- filtered_df %>% 
  filter(perm_type == "Permuted") %>% 
  select(pleiotropy_score, max_protein_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test2 <- paste("'Permuted'","~R^2==~",round(cor_unfilt[2]^2, digits = 3))

exp2_protein <- ggplot(filtered_df, aes(x = max_protein_expression_23tissues, y = pleiotropy_score, color = perm_type)) +
  geom_smooth(method = lm) + 
  labs(x = "Max protein expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Field") +
  annotate(geom="text", label = test1, x = r2_text_x, y = .37, size = annotate_size, parse = TRUE, hjust = 0) +
  annotate(geom="text", label = test2, x = r2_text_x, y = .34, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=cbPalette) +
  scale_fill_manual(values=cbPalette) +
  labs(tag = "B") 


# goodman metabolite ------------------------------------------------------------------

filtered_df <- all_interval_data_melted %>% filter(pop_trait == "Goodman Metabolite")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(max_protein_expression_23tissues ~ pleiotropy_score, temp1))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(max_protein_expression_23tissues ~ pleiotropy_score, temp2))

cor_filt <- filtered_df %>% 
  filter(perm_type == "Observed Data") %>% 
  select(pleiotropy_score, max_protein_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test1 <- paste("'Observed'","~R^2==~",round(cor_filt[2]^2, digits = 3))
cor_unfilt <- filtered_df %>% 
  filter(perm_type == "Permuted") %>% 
  select(pleiotropy_score, max_protein_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test2 <- paste("'Permuted'","~R^2==~",round(cor_unfilt[2]^2, digits = 3))

exp3_protein <- ggplot(filtered_df, aes(x = max_protein_expression_23tissues, y = pleiotropy_score, color = perm_type)) +
  geom_smooth(method = lm) + 
  labs(x = "Max protein expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Mass Features") +
  annotate(geom="text", label = test1, x = r2_text_x, y = 8.2, size = annotate_size, parse = TRUE, hjust = 0) +
  annotate(geom="text", label = test2, x = r2_text_x, y = 7.6, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=cbPalette) +
  scale_fill_manual(values=cbPalette) +
  labs(tag = "C") 


# goodman expression ------------------------------------------------------------------

filtered_df <- all_interval_data_melted %>% filter(pop_trait == "Goodman Expression")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(max_protein_expression_23tissues ~ pleiotropy_score, temp1))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(max_protein_expression_23tissues ~ pleiotropy_score, temp2))

cor_filt <- filtered_df %>% 
  filter(perm_type == "Observed Data") %>% 
  select(pleiotropy_score, max_protein_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test1 <- paste("'Observed'","~R^2==~",round(cor_filt[2]^2, digits = 3))
cor_unfilt <- filtered_df %>% 
  filter(perm_type == "Permuted") %>% 
  select(pleiotropy_score, max_protein_expression_23tissues) %>%
  cor() %>% 
  round(digits = 3)
test2 <- paste("'Permuted'","~R^2==~",round(cor_unfilt[2]^2, digits = 3))

exp4_protein <- ggplot(filtered_df, aes(x = max_protein_expression_23tissues, y = pleiotropy_score, color = perm_type)) +
  geom_smooth(method = lm) + 
  labs(x = "Max protein expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Expression") +
  annotate(geom="text", label = test1, x = r2_text_x, y = 14.5, size = annotate_size, parse = TRUE, hjust = 0) +
  annotate(geom="text", label = test2, x = r2_text_x, y = 13, size = annotate_size, parse = TRUE, hjust = 0) +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  scale_colour_manual(values=cbPalette) +
  scale_fill_manual(values=cbPalette) +
  labs(tag = "D")  


# Look at all plots together ------------------------------------------------------------
# ggpubr::ggarrange(exp1_protein,exp2_protein,exp3_protein,exp4_protein, nrow = 2, ncol = 2,
#                   common.legend = TRUE, legend = "bottom", align = "v")

# Export to file ------------------------------------------------------------------------
ggsave("~/git_projects/pleiotropy/images/adjusted_pleiotropy_vs_protein_expression.png",
       plot = ggpubr::ggarrange(exp1_protein,exp2_protein,exp3_protein,exp4_protein, nrow = 2, ncol = 2,
                                common.legend = TRUE, legend = "bottom", align = "v"),
       width = 6.5,
       height = 5.5, 
       units = "in",
       dpi = "retina")




# ------------------------------------------------------------------------------------------------------
# Plot All expression values for each population-trait category
# A horrible mess but here if anyone asks
# ------------------------------------------------------------------------------------------------------

# remove protein data
all_interval_data_rna <- all_interval_data_melted %>% filter(is_adjusted == "Residual")
all_interval_data_rna <- all_interval_data_rna[,-c(2:34,59:63,67:69,71,72)]

all_interval_data_melted_rna <- tidyr::pivot_longer(all_interval_data_rna,
                                                    cols = Ear_Primordium_6_8_mm_rna:max_rna_expression_23tissues,
                                                    names_to = "tissue",
                                                    values_to = "rna_value")


# nam physiological --------------------------------------------------------------------
filtered_df <- all_interval_data_rna %>% filter(pop_trait == "NAM Physiological")
dim(filtered_df)

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(pleiotropy_score~., temp1[,-c(1,26,28,29)]))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(pleiotropy_score~.,  temp2[,-c(1,26,28,29)]))

filtered_df_melted <- all_interval_data_melted_rna %>% filter(pop_trait == "NAM Physiological" & perm_type == "Observed Data")
exp1_all <- ggplot(filtered_df_melted, aes(x = rna_value, y = pleiotropy_score, color = tissue)) +
  geom_smooth(method = lm) + 
  labs(x = "RNA expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "NAM Field") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  labs(tag = "A") 


# goodman physiological ----------------------------------------------------------------

filtered_df <- all_interval_data_rna %>% filter(pop_trait == "Goodman Physiological")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(pleiotropy_score~., temp1[,-c(1,26,28,29)]))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(pleiotropy_score~.,  temp2[,-c(1,26,28,29)]))

filtered_df_melted <- all_interval_data_melted_rna %>% filter(pop_trait == "Goodman Physiological" & perm_type == "Observed Data")
exp2_all <-  ggplot(filtered_df_melted, aes(x = rna_value, y = pleiotropy_score, color = tissue)) +
  geom_smooth(method = lm) + 
  labs(x = "RNA expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Field") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  labs(tag = "B") 


# goodman metabolite ------------------------------------------------------------------

filtered_df <- all_interval_data_rna %>% filter(pop_trait == "Goodman Metabolite")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(pleiotropy_score~.,  temp2[,-c(1,26,28,29)]))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(pleiotropy_score~.,  temp2[,-c(1,26,28,29)]))

filtered_df_melted <- all_interval_data_melted_rna %>% filter(pop_trait == "Goodman Metabolite" & perm_type == "Observed Data")
exp3_all <-  ggplot(filtered_df_melted, aes(x = rna_value, y = pleiotropy_score, color = tissue)) +
  geom_smooth(method = lm) + 
  labs(x = "RNA expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Mass Features") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  labs(tag = "C") 


# goodman expression ------------------------------------------------------------------

filtered_df <- all_interval_data_rna %>% filter(pop_trait == "Goodman Expression")

# Observed Data data model
temp1 <- filtered_df %>% filter(perm_type == "Observed Data")
summary(lm(pleiotropy_score~., temp1[,-c(1,26,28,29)]))

# Permuted data model
temp2 <- filtered_df %>% filter(perm_type == "Permuted")
summary(lm(pleiotropy_score~.,  temp2[,-c(1,26,28,29)]))

filtered_df_melted <- all_interval_data_melted_rna %>% filter(pop_trait == "Goodman Expression" & perm_type == "Observed Data")
exp4_all <-  ggplot(filtered_df_melted, aes(x = rna_value, y = pleiotropy_score, color = tissue)) +
  geom_smooth(method = lm) + 
  labs(x = "RNA expression across 23 B73 tissues", 
       y = "Adjusted Pleiotropy", 
       title = "GAP Expression") +
  theme_classic() +
  theme(axis.text=element_text(size=axis_text_size), 
        axis.title = element_text(size=axis_text_size),
        plot.title = element_text(size=title_text_size), 
        legend.position="bottom", 
        legend.text = element_text(size = legend_text_size),
        legend.title=element_blank(),
        legend.key.size = unit(legend_shape_size, 'cm'),
        plot.tag=element_text(size=tag_size)) +
  labs(tag = "D")  


# Look at all plots together ------------------------------------------------------------
# ggpubr::ggarrange(exp1_all,exp2_all,exp3_all,exp4_all, nrow = 2, ncol = 2, 
#                   common.legend = TRUE, legend = "bottom", align = "v")

# Export to file ------------------------------------------------------------------------
ggsave("~/git_projects/pleiotropy/images/adjusted_pleiotropy_vs_rna_expression_all_tissues.png",
       plot = ggpubr::ggarrange(exp1_all,exp2_all,exp3_all,exp4_all, nrow = 2, ncol = 2, 
                                common.legend = TRUE, legend = "bottom", align = "v"),
       width = 15,
       height = 10, 
       units = "in")
